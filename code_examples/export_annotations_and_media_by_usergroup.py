#!/usr/bin/env python3
"""
Script to export all annotation_sets, media_collections and associated media objects from the SQ+ API.

Usage:
    export_annotations_and_media_by_usergroup.py [-h] [--host HOST] [--api_token API_TOKEN] [--group_id GROUP_ID] [--output_directory OUTPUT_DIRECTORY]

Export annotations and associated media by usergroup. It includes an interactive prompt to step through the download process.

optional arguments:
  -h, --help                            show this help message and exit
  --host HOST                           The host/instance you're pointing to (default: https://squidle.org)
  --api_token API_TOKEN                 The API token for your user account (omit for interactive prompt)
  --group_id GROUP_ID                   The ID of the group that you want to export (omit for interactive prompt)
  --output_directory OUTPUT_DIRECTORY   Location of where to save exported data (omit for interactive prompt)

Note:
    i)  if you're not using the squidle.org instance, you'll need to specify the "--host" parameter. The other parameters
        will be requested through an interactive prompt, if not supplied.
    ii) if you don't already have it, you'll need the "requests" python module. Install it with:
        pip install requests
"""
# get dependencies, note code below is python >=3.6
# pip install requests # to install request module if you don't have it
import re
import os
import json
try: import requests
except: print("Requests module is not found. Try installing with\n\npip install requests\n")

DEFAULT_HOST = "https://squidle.org"


class SQDownloader:
    def __init__(self, output_directory, api_token, host=DEFAULT_HOST):
        self.api_token = api_token
        self.host = host
        self.output_directory = output_directory

        # make sure that the output_directory is valid
        assert os.path.isdir(output_directory), f"Invalid output directory: '{output_directory}' doesn't exist..."

    def export_media_collection(self, media_collection_id):
        """Download a CSV file and JSON metadata for a given media_collection"""
        include_columns = ["id", "key", "path_best", "timestamp_start", "pose.timestamp", "pose.lat", "pose.lon","pose.alt", "pose.dep", "pose.data", "pose.id", "deployment.key", "deployment.campaign.key","deployment.id", "deployment.campaign.id"]
        f = dict(operations=[dict(module="pandas", method="json_normalize")])  # file operations
        url = f'{self.host}/api/media_collection/{media_collection_id}/export?template=dataframe.csv&disposition=attachment&f={json.dumps(f)}&include_columns={json.dumps(include_columns)}'
        self.download_file(url, os.path.join(self.output_directory, "media_collections"))

    def export_annotation_set(self, annotation_set_id):
        """Download a CSV file and JSON metadata for a given annotation_set"""
        # include_columns = ["label.id","label.uuid","label.name","label.lineage_names","comment","tag_names","updated_at","user.username","object_id","id","point.id","point.x","point.y","point.t","point.data","point.media.id","point.media.key"]
        f = dict(operations=[dict(module="pandas", method="json_normalize")])  # file operations
        q = dict(filters=[dict(name="label_id", op="is_not_null")])  # exclude unlabeled annotations
        # url = f'{self.host}/api/annotation_set/{annotation_set_id}/export?template=dataframe.csv&disposition=attachment&f={json.dumps(f)}&q={json.dumps(q)}&include_columns={json.dumps(include_columns)}'
        url = f'{self.host}/api/annotation_set/{annotation_set_id}/export?template=dataframe.csv&disposition=attachment&f={json.dumps(f)}&q={json.dumps(q)}'
        self.download_file(url, os.path.join(self.output_directory, "annotation_sets"))

    def download_media_objects(self, media_collection_id):
        """Download media objects for a given media collection"""
        include_columns = ["key", "path_best", "timestamp_start", "deployment.key", "deployment.campaign.key"]
        url = f'{self.host}/api/media_collection/{media_collection_id}/export?include_columns={json.dumps(include_columns)}'
        data = requests.get(url, headers={'X-auth-token': self.api_token}).json()

        # create dir to save using name of media_collection
        save_media_dir = os.path.join(self.output_directory, "media_collection_media", data.get("metadata", {}).get("name"))
        os.makedirs(save_media_dir, exist_ok=True)

        # iterate through, download & save images
        for i in data.get("objects"):
            mpath = i.get('path_best')
            if mpath.startswith("/"):    # if relative path, add host to make absolute
                mpath = self.host + mpath
            fname = os.path.basename(mpath.strip('/'))  # remove trailing slashes on filenames (hack fix for RLS images)
            with open(os.path.join(save_media_dir, fname), 'wb') as handler:
                handler.write(requests.get(mpath).content)

    def download_file(self, url, save_dir):
        """Download CSV and Metadata (if exists) for a URL"""
        r = requests.get(url, headers={'X-auth-token': self.api_token})
        fname = re.findall("filename=(.+)", r.headers['content-disposition'])[0]    # get filename from response header
        os.makedirs(save_dir, exist_ok=True)
        with open(os.path.join(save_dir, fname), 'wb') as csv_file:
            csv_file.write(r.content)
        # Save extra metadata if it exists
        metadata = r.headers.get("X-Content-Metadata", None)   # special response header for SQAPI
        if metadata is not None:
            with open(os.path.join(save_dir, fname+"-metadata.json"), "w") as metajson:
                metajson.write(json.dumps(json.loads(metadata), indent=4))

    def get_annotation_sets_by_group(self, group_id):
        """Get a list of annotation_sets shared in a group"""
        q = dict(filters=[dict(name="usergroups", op="any", val=dict(name="id", op="eq", val=group_id))])
        url = f'{self.host}/api/annotation_set?q={json.dumps(q)}&results_per_page=100'
        return requests.get(url, headers={'X-auth-token': self.api_token}).json()

    def get_annotation_set_group_list(self):
        """Get a list of my groups that share annotation_sets"""
        q = {'filters': [{"name": "shared_annotation_sets", "op": "any", "val": {"name": "id", "op": "is_not_null"}},
                         {"or": [{"name": "current_user_is_owner", "op": "eq", "val": True},
                                 {"name": "current_user_is_member", "op": "eq", "val": True},
                                 {"name": "is_public", "op": "eq", "val": True}]}],
             "order_by": [{"field": "name", "direction": "asc"}]}
        url = f'{self.host}/api/groups?q={json.dumps(q)}&results_per_page=100'
        return requests.get(url, headers={'X-auth-token': self.api_token}).json()


if __name__ == "__main__":
    import getpass
    import argparse
    import webbrowser   # to open directory after processing
    import sys

    # Build CLI input arguments
    parser = argparse.ArgumentParser(description="Export annotations and associated media by usergroup. It includes an interactive prompt to step through the download process.")
    parser.add_argument("--host", default=DEFAULT_HOST, help=f"The host/instance you're pointing to (default: {DEFAULT_HOST})")
    parser.add_argument("--api_token", default=None, help="The API token for your user account (omit for interactive prompt)")
    parser.add_argument("--group_id", type=int, default=None, help="The ID of the group that you want to export (omit for interactive prompt)")
    parser.add_argument('--output_directory', default=None, help="Location of where to save exported data (omit for interactive prompt)")

    # Get CLI input arguments, or prompt for them if not supplied
    args = parser.parse_args()

    # Prompt for API token, if not supplied
    api_token = args.api_token if args.api_token else getpass.getpass("Enter your API token: ")

    # Open file dialog to select output directory, if not supplied
    if args.output_directory is not None:
        output_directory = args.output_directory
    else:
        # Open filedialog to select directory
        from tkinter import Tk, filedialog
        root = Tk()  # pointing root to Tk() to use it as Tk() in program.
        root.withdraw()  # Hides small tkinter window.
        root.attributes('-topmost', True)  # Opened windows will be active. above all windows despite of selection.
        print('Please select an output directory using the file dialog')
        output_directory = filedialog.askdirectory(initialdir="/",  title='Please select an output directory')

    # initialise the downloader instance
    api = SQDownloader(output_directory=output_directory, host=args.host, api_token=api_token)

    # Get list of groups and prompt for group ID, if not supplied
    if args.group_id:
        group_id = args.group_id
    else:
        mygroups = api.get_annotation_set_group_list()
        print("\nYour groups that have annotation_sets:")
        for g in mygroups.get("objects", []):
            print(f" [ID: {g.get('id')}] {g.get('name')}")
        group_id = input("Enter the ID of the group that you'd like to export: ")

    # Get annotation_sets from nominated group
    annotation_sets = api.get_annotation_sets_by_group(group_id)
    print(f"Found {annotation_sets.get('num_results', 0)} annotation_sets to process...\nSaving to: {output_directory}")

    # Export annotation_sets
    media_collections = []
    for a in annotation_sets.get("objects", []):
        print(f" * Exporting CSV for annotation_set: {a.get('name')}...")
        api.export_annotation_set(a.get("id"))
        media_collections.append(a.get("media_collection", {}))

    # Get unique list of dicts (since there may be more than one annotation_set per media_collection)
    media_collections = list({v['id']: v for v in media_collections}.values())

    # Export media_collections and all media objects
    for mc in media_collections:
        print(f" * Exporting CSV for media_collection: {mc.get('name')}...")
        api.export_media_collection(mc.get("id"))
        print(f" - Downloading media objects for media_collection: {mc.get('name')}...")
        api.download_media_objects(mc.get("id"))

    print(f"\nData has been saved to:\n{output_directory}")
    webbrowser.open('file://'+output_directory if sys.platform == 'darwin' else output_directory)


