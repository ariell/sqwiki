#!/usr/bin/env python3
"""
Script to export all annotation_sets, media_collections and associated media objects from the SQ+ API.

Usage:
    export_annotations_and_media_by_usergroup.py [-h] [--host HOST] [--api_token API_TOKEN] [--group_id GROUP_ID] [--output_directory OUTPUT_DIRECTORY]

Export annotations and associated media by usergroup. It includes an interactive prompt to step through the download process.

optional arguments:
  -h, --help                            show this help message and exit
  --host HOST                           The host/instance you're pointing to (default: https://squidle.org)
  --api_token API_TOKEN                 The API token for your user account (omit for interactive prompt)
  --group_id GROUP_ID                   The ID of the group that you want to export (omit for interactive prompt)
  --output_directory OUTPUT_DIRECTORY   Location of where to save exported data (omit for interactive prompt)

Note:
    i)  if you're not using the squidle.org instance, you'll need to specify the "--host" parameter. The other parameters
        will be requested through an interactive prompt, if not supplied.
    ii) if you don't already have it, you'll need the "requests" python module. Install it with:
        pip install requests
"""
# get dependencies, note code below is python >=3.6
# pip install requests # to install request module if you don't have it
import re
import os
import json
try: import requests
except: print("Requests module is not found. Try installing with\n\npip install requests\n")

DEFAULT_HOST = "https://squidle.org"
# DEFAULT_HOST = "http://localhost:5000"


class SQImportAnnotations:
    def __init__(self, api_token, host=DEFAULT_HOST):
        self.api_token = api_token
        self.host = host

    def create_media_collection(self, name, description=None):
        url = f"{self.host}/api/media_collection"
        data = dict(name=name, description=description)
        print("\nCreating media_collection...")
        r = requests.post(url, json=data, headers={'X-auth-token': self.api_token}).json()
        print(json.dumps(r, indent=2))
        return r

    def create_annotation_set(self, name, media_collection_id, label_scheme_id, description=None, **kwargs):
        url = f"{self.host}/api/annotation_set"
        data = dict(name=name, description=description, media_collection_id=media_collection_id, label_scheme_id=label_scheme_id, **kwargs)
        print("Creating annotation_set...")
        r = requests.post(url, json=data, headers={'X-auth-token': self.api_token}).json()
        print(json.dumps(r, indent=2))
        return r

    def get_label_scheme_id(self):
        r = requests.get(f"{self.host}/api/label_scheme?results_per_page=100", headers={'X-auth-token': self.api_token}).json()
        for o in r.get("objects"):
            print(f"[ID: {o.get('id'):<3}] {o.get('name')}")
        return int(input("Enter the ID of label_scheme:\n"))

    def get_annotation_set(self, annotation_set_id):
        url = f"{self.host}/api/annotation_set/{annotation_set_id}"
        r = requests.get(url, headers={'X-auth-token': self.api_token}).json()
        return r

    def create_annotation_set_file(self, file_url, annotation_set_id, description=None):
        url = f"{self.host}/api/annotation_set_file"
        data = dict(file_url=file_url, description=description, annotation_set_id=annotation_set_id)
        print("Creating annotation_set_file...")
        r = requests.post(url, json=data, headers={'X-auth-token': self.api_token}).json()
        print(json.dumps(r, indent=2))
        return r

    def import_annotations(self, annotation_set_file, f=None, pixel_width=None, pixel_height=None, deployment_id=None, skip_errors=False):
        url = f"{self.host}/api/annotation_set_file/{annotation_set_file.get('id')}/data"
        # save = {"collection": "deployment.media", "update_existing": True, "match_on": ["key"], "create_missing": True, "skip_errors": True}
        save = {"collection": "annotation_set.points", "skip_errors": skip_errors}
        if f is None:
            f = {"operations": [
                {"module": "pandas", "method": "read_csv", "kwargs": {
                    "skipinitialspace": True, "dtype": {"set_label.origin_code": "str"}}, 'keep_default_na': False},
                # TODO: if not in correct format, add fileops here
                {"method": "to_dict", "kwargs": {"orient": "records"}},
                {"module": "data", "method": "unflatten_dicts", "kwargs": {}}
            ]}
        url = f"{url}?save={json.dumps(save)}&f={json.dumps(f)}&annotation_set_id={annotation_set_file.get('annotation_set_id')}"
        if pixel_width and pixel_height:
            url += f"&pixel_width={pixel_width}&pixel_height={pixel_height}"
        if deployment_id:
            url += f"&deployment_id={deployment_id}"
        print(f"Importing annotations...\nURL: {url}")
        return requests.get(url, headers={'X-auth-token': self.api_token}).json()


if __name__ == "__main__":
    import getpass
    import argparse

    # Build CLI input arguments
    parser = argparse.ArgumentParser(description="Import annotations from file.")
    parser.add_argument("--host", default=DEFAULT_HOST, help=f"The host/instance you're pointing to (default: {DEFAULT_HOST})")
    parser.add_argument("--dataset_name", default=None, help="The name of the media_collection/annotation_set (omit for interactive prompt, ignored if annotation_set_id is set)")
    parser.add_argument('--file_url', default=None, help="Location of where to save exported data (omit for interactive prompt)")
    parser.add_argument("--api_token", default=None, help="The API token for your user account (omit for interactive prompt)")
    parser.add_argument("--label_scheme_id", type=int, default=None, help="ID of the label_scheme to use (omit for interactive prompt, ignored if annotation_set_id is set)")
    parser.add_argument("--annotation_set_id", type=int, default=None, help="ID of existing annotation_set to add labels to")
    parser.add_argument("--pixel_width", type=int, default=None, help="OPTIONAL Width of media object to scale coordinates")
    parser.add_argument("--pixel_height", type=int, default=None, help="OPTIONAL Width of media object to scale coordinates")
    parser.add_argument("--deployment_id", type=int, default=None, help="OPTIONAL ID of deployment to speed up media matching if all from same deployment")
    parser.add_argument('--skip_errors', action='store_true', default=False, help="DEFAULT False, whether or not to skip errors or stop. May lead to partial imports.")

    # Get CLI input arguments, or prompt for them if not supplied
    args = parser.parse_args()

    # Prompt for API token, if not supplied
    api_token = args.api_token if args.api_token else getpass.getpass("Enter your API token: ")

    # initialise the downloader instance
    api = SQImportAnnotations(host=args.host, api_token=api_token)

    # get the file_url, prompt for it if not supplied
    file_url = args.file_url if args.file_url else input("Enter the URL of the csv file to import:\n")

    annotation_set_id = args.annotation_set_id

    if annotation_set_id is None:
        # get the name of the dataset, prompt for it if not supplied
        dataset_name = args.dataset_name if args.dataset_name else input("Enter name of Dataset:\n")

        # get the label_scheme, prompt for it if not supplied
        label_scheme_id = args.label_scheme_id if args.label_scheme_id else api.get_label_scheme_id()

        # do the stuff: create datasets, import file, and process annotations
        description = f"Dataset created by importing annotations from {file_url}"
        media_collection = api.create_media_collection(dataset_name, description=description)
        annotation_set = api.create_annotation_set(dataset_name, media_collection.get("id"), label_scheme_id, description=description)
        annotation_set_id = annotation_set.get("id")
    annotation_set_file = api.create_annotation_set_file(file_url, annotation_set_id=annotation_set_id)
    resp = api.import_annotations(annotation_set_file, pixel_width=args.pixel_width, pixel_height=args.pixel_height, deployment_id=args.deployment_id, skip_errors=args.skip_errors)
    json.dump(resp, open('result.json', 'w'))
    del resp['error_rows']
    print(json.dumps(resp, indent=2))

