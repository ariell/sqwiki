#!/usr/bin/env python3
"""
Script to copy the annotations from an annotation_set using the SQ+ API.

Usage:
    copy_annotation_set.py [-h] [--host HOST] [--dataset_name DATASET_NAME] [--api_token API_TOKEN] --annotation_set_id ANNOTATION_SET_ID
                              [--skip_errors]

optional arguments:
  -h, --help            show this help message and exit
  --host HOST           The host/instance you're pointing to (default: https://squidle.org)
  --dataset_name DATASET_NAME
                        The name of the media_collection/annotation_set (omit for interactive prompt, ignored if annotation_set_id is set)
  --api_token API_TOKEN
                        The API token for your user account (omit for interactive prompt)
  --annotation_set_id ANNOTATION_SET_ID
                        ID of existing annotation_set to add labels to
  --skip_errors         DEFAULT False, whether or not to skip errors or stop. May lead to partial imports.

Note:
    i)  if you're not using the squidle.org instance, you'll need to specify the "--host" parameter. The other parameters
        will be requested through an interactive prompt, if not supplied.
    ii) if you don't already have it, you'll need the "requests" python module. Install it with:
        pip install requests
"""
# get dependencies, note code below is python >=3.6
# pip install requests # to install request module if you don't have it
import re
import os
import json
try: import requests
except: print("Requests module is not found. Try installing with\n\npip install requests\n")

DEFAULT_HOST = "https://squidle.org"
# DEFAULT_HOST = "http://localhost:5000"

from import_annotations import SQImportAnnotations

if __name__ == "__main__":
    import getpass
    import argparse

    # Build CLI input arguments
    parser = argparse.ArgumentParser(description="Import annotations from file.")
    parser.add_argument("--host", default=DEFAULT_HOST, help=f"The host/instance you're pointing to (default: {DEFAULT_HOST})")
    parser.add_argument("--dataset_name", default=None, help="The name of the media_collection/annotation_set (omit for interactive prompt, ignored if annotation_set_id is set)")
    parser.add_argument("--api_token", default=None, help="The API token for your user account (omit for interactive prompt)")
    parser.add_argument("--src_annotation_set_id", type=int, default=None, help="ID of annotation_set to be copied")
    parser.add_argument("--dest_annotation_set_id", type=int, default=None, help="[OPTIONAL] ID of existing annotation_set to add labels to. If not supplied, a new one will be created automatically.")
    parser.add_argument('--skip_errors', action='store_true', default=False, help="DEFAULT False, whether or not to skip errors or stop. May lead to partial imports.")

    # Get CLI input arguments, or prompt for them if not supplied
    args = parser.parse_args()

    # Prompt for API token, if not supplied
    api_token = args.api_token if args.api_token else getpass.getpass("Enter your API token: ")

    # Prompt annotation_set ID
    src_annotation_set_id = args.src_annotation_set_id if args.src_annotation_set_id else input("Enter the ID of the annotation_set you wish to copy\n")

    # initialise the downloader instance
    api = SQImportAnnotations(host=args.host, api_token=api_token)

    # get the file_url, prompt for it if not supplied
    includes = ["label.id", "comment", "needs_review", "likelihood", "point.x", "point.y", "point.t", "point.polygon",
                "point.media.id", "tag_names"]
    file_ops = dict(operations=[dict(module="pandas", method="json_normalize"), dict(method="sort_index", kwargs={"axis": 1}),
                dict(method="rename", kwargs={'columns': {
                    "comment": "annotation_label.comment",
                    "label.id": "annotation_label.id",
                    "likelihood": "annotation_label.likelihood",
                    "needs_review": "annotation_label.needs_review",
                    "tag_names": "annotation_label.tag_names",
                    "point.media.id": "media_id",
                    "point.x": "x",
                    "point.y": "y",
                    "point.polygon": "polygon"
                }})])

    file_url = f'{args.host}/api/annotation_set/{src_annotation_set_id}/export' \
               f'?template=dataframe.csv&include_columns={json.dumps(includes)}&f={json.dumps(file_ops)}&index=false'


    # Get the source annotation set that is being copied
    src_annotation_set = api.get_annotation_set(src_annotation_set_id)

    # get the name of the dataset, prompt for it if not supplied
    if args.dest_annotation_set_id:
        dest_annotation_set_id = args.dest_annotation_set_id
    else:
        dataset_name = src_annotation_set.get("name") + "-copy"

        # get the label_scheme, prompt for it if not supplied
        label_scheme_id = src_annotation_set.get("label_scheme",{}).get("id")

        data = dict(data=src_annotation_set.get("data"), is_full_bio_score=src_annotation_set.get("is_full_bio_score"),
                    is_real_science=src_annotation_set.get("is_real_science"), is_qaqc=src_annotation_set.get("is_qaqc"))

        # do the stuff: create datasets, import file, and process annotations
        description = f"Copy of {src_annotation_set.get('name')} [ID:{src_annotation_set.get('id')}]"
        media_collection = src_annotation_set.get("media_collection")
        annotation_set = api.create_annotation_set(dataset_name, media_collection.get("id"), label_scheme_id, description=description, **data)
        dest_annotation_set_id = annotation_set.get("id")
    annotation_set_file = api.create_annotation_set_file(file_url, annotation_set_id=dest_annotation_set_id)
    resp = api.import_annotations(annotation_set_file, skip_errors=args.skip_errors)
    json.dump(resp, open('result.json', 'w'))
    del resp['error_rows']
    print(json.dumps(resp, indent=2))
