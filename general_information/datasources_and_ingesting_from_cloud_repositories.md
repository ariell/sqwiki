# Datasources: Ingesting survey data from Cloud Storage Repositories

The sections on this page include:

[TOC]

Survey data can be imported from a variety of preexisting cloud storage repositories (i.e., Datasource Repositories).
Each supported Datasource Repository requires a Datasource Plugin that enables the system to read data from the
repository. Each Platform (e.g., ROV, AUV) requires at least one Datasource Definition.

Different data structures hosted on a variety online repository architectures are supported (e.g., AWS, GoogleCloud,
Apache HTTP directory listing, Thredds, Microsoft Azure and even custom APIs). Leveraging existing data in existing
repositories saves costs and prevents duplicating quality control and processing efforts, as well as divergence of
sources and downstream products. Moreover, in order to ensure long-term functionality, SQUIDLE+ requires the Datasource
Repositories to be:

1. **Persistent**. The repository is persistent. I.e., once the imagery is linked, the underlying media item must not
   change or disappear.
2. **Consistent**. Once defined, the data format for a data source or platform will not change. Flexible data formats
   are supported by flexible plugins that handle multiple and new definitions.
3. **Accessible**. The media items (e.g., images) and thumbnails must be accessible through a publicly available (
   read-only) link. It is possible to set up authenticated proxies but this is more complex, slows things down and can
   circumvent any access restrictions, so it's usually better to just provide public links.

SQUIDLE+ expedites data delivery by enabling Data Providers to focus on managing their own Datasource Repository, with
the corresponding SQUIDLE+ Datasource Plugin automatically handling imports into SQUIDLE+.

For more info on supported *Cloud Storage Repositories*, *Datasource Plugins* and example *Datasource definitions*, see
the [Data source definitions](data_source_definitions.md).

## Datasource Plugins & Datasource Definitions

The integration of Datasource Repositories is facilitated through Datasource Plugins. These plugins enable a consistent
Datasource Definition between different Datasource Repositories. Each Datasource Definition is parameterised in the same
way, defining customizable search patterns specifying where data should be found in the chosen repository structure.
Each can reference a different Datasource Plugin defined by the `datasource_type` model. The plugin contains all
the necessary code to browse and import from the selected repository. The plugins standardise the parameters making the
data portable across repositories. The table below provides an overview of the parameters for a Datasource Definition.

PARAMETER | DESCRIPTION
--- | ---
`urlbase_browse` | URL base pattern to list repository objects
`datafile_pattern` | URL path pattern to define link to datafile for download
`datafile_operations` | Sequence of transformations to apply to the data file(s) to convert it into the format required by the API
`campaign_search` | Campaign path search pattern.
`deployment_search` | Deployment path search pattern
`media_pattern` | Media path pattern to match. This is a URL build up from `campaign`, `deployment` and/or `media` properties.
`thumb_pattern` | [Optional] Thumbnail path pattern to match. This is a URL build up from `campaign`, `deployment` and/or `media` properties. If left blank, the system will dynamically generate and cache a thumbnail. Note, however that this can be slow and it is highly recommended to store thumbnails on the Datasource Repository
`datafile_search` | [Optional] Data file path pattern to match (if cannot be exactly matched, eg: contains variable date, or deployment name).
`mediadir_search` | [Optional] Media dir pattern to match (if cannot be exactly matched, eg: contains variable date).
`thumbdir_search` | [Optional] Thumbnail dir pattern match (if cannot be exactly matched, eg: contains variable date).

The `datafile_operations` parameter defines a sequence of data transformation operations that can be used to prepare the
uploaded datasets for import. These operations pre-process the raw input in the repository into the format required by
the API, provided the format is consistent for a given Datasource. This makes it possible to work with most existing
data formats without requiring further processing of the data to import it into SQUIDLE+. These are applied to all
datasets from a given Datasource Repository upon import.

Once a Datasource Definition, Datasource Plugin and Datasource Repository have been configured, new datasets that are
uploaded to the Datasource Repository that conform to the Datasource Definition can be easily ingested into SQUIDLE+.
Once the imports have been tested and verified, it is possible to run imports as a scheduled service that can
synchronise against the API periodically. This allows a "set-and-forget" workflow where any new datasets uploaded to the
Datasource Repository will automatically be ingested and made available through SQUIDLE+.

### Supported hosts & repositories 
* Amazon Web Services (AWS) S3 through XML API
* Google Cloud Storage (GCS) through JSON API
* Apache directory listings
* Local file system (and NFS mounts)
* THREDDS Data Server (TDS)
* Custom, RLS API
* Microsoft Azure Blob (coming soon)

Adding additional supported repositories is relatively straight forward through `Datasource Plugins`. 

> **NOTE**: Normally Datasource Plugins and Datasource Definitions are normally be set up with support from our development team.

## Repository structure

The structure of the Datasource Repository refers to the organisation of the files within the repository. The properties
for the data structure are outlined in the table below:

PROPERTY | DESCRIPTION
---- | ----
`PLATFORM_NAME` | name of the platform (eg: the ROV,AUV, towcam platform name, not used on import, but necessary to keep repository organised if multiple platforms)
`CAMPAIGN_NAME` | name of a campaign / cruise (used to define campaign name on import)
`DEPLOYMENT_NAME` | name of the deployment or dive (used to define deployment name on import)
`images/` |  directory containing media objects (eg: images, can be called something else)
`thumbnails/` |  directory containing media object thumbnails (can be called something else). It is possible dynamically generate image thumbnails, but it is highly recommended that thumbnails are provided by the repository.
`navdata.csv` | file containing  position info including references to image files in images/ directory (can be called something else)

#### Example 1 (recommended)

This structure allows for multiple platforms per campaign, which can often be the case in multi-vehicle ops.

* `CLOUD_STORAGE_REPOSITORY/`
    * `CAMPAIGN_NAME/`
        * `PLATFORM_NAME/`
            * `DEPLOYMENT_NAME/`
                * `images/`
                    * `ROV_20100625T130726_8888.jpg`
                    * `ROV_20100625T130727_7234.jpg`
                    * `...`
                * `thumbnails/`
                    * `ROV_20100625T130726_8888_THM.jpg`
                    * `ROV_20100625T130727_7234_THM.jpg`
                    * `...`
                * `navdata.csv`

#### Example 2

This structure may be useful for a single platform repository

* `CLOUD_STORAGE_REPOSITORY/`
    * `PLATFORM_NAME/`
        * `CAMPAIGN_NAME/`
            * `DEPLOYMENT_NAME/`
                * `images/`
                    * `ROV_20100625T130726_8888.jpg`
                    * `ROV_20100625T130727_7234.jpg`
                    * `...`
                * `thumbnails/`
                    * `ROV_20100625T130726_8888_THM.jpg`
                    * `ROV_20100625T130727_7234_THM.jpg`
                    * `...`
                * `navdata.csv`

#### Example 3

This structure may be useful for a single platform repository

* `CLOUD_STORAGE_REPOSITORY_FOR_PLATFORM/`
    * `CAMPAIGN_NAME/`
        * `DEPLOYMENT_NAME/`
            * `images/`
                * `ROV_20100625T130726_8888.jpg`
                * `ROV_20100625T130727_7234.jpg`
                * `...`
            * `thumbnails/`
                * `ROV_20100625T130726_8888_THM.jpg`
                * `ROV_20100625T130727_7234_THM.jpg`
                * `...`
            * `navdata.csv`

#### General info on data structures

For a single Platform repository, the `PLATFORM_NAME` level can be omitted. Different data structures are supported and
can be configured through the search patterns outlined in the Datasource Definition. It is worth noting that the data
structure is used to infer the
`CAMPAIGN_NAME` and `DEPLOYMENT_NAME` for each dataset using the Datasource Definitions, so it is important that this
structure is consistent and organised in an appropriate way so these attributes can be mapped back to the Campaign and
Deployment models, respectively.

It is also possible to create a more complex pipeline from custom APIs or WFS services to dynamically generate a
`navdata.csv` file for import. This can be achieved through adding additional preprocessing steps that can extend an
existing Datasource Plugin, or by creating custom Datasource Plugins. However, these more complex arrangements require
custom software integration, and sticking to a consistent, compatible data structure with an already supported
Datasource Plugin makes it straightforward to import new data.

## Example `navdata.csv` file: ##

An example of the CSV file. Note: filename and column heading can be different. The format is specified upon setting up
the data source as a data source definition. We can define data transformation operations for the data source to convert
the file into the expected format, but it is important that whatever format is chosen, remains consistent.

| key  | pose.lat | pose.lon | pose.dep | pose.alt | timestamp_start | pose.data.sen1 | pose.data.sen2 | data.key1 | data.key2
| --- | --- | --- | --- | --- | --- | --- | --- |--- | --- |
| ROV_20100625T130726_8888  | 23.123456  | 12.123456 | 95.0 | 2.0 | 2010-06-25 13:07:26.8888 | 3.4 | 8.3 | something | interesting
| ROV_20100625T130727_7234  | 23.234567  | 12.234567 | 95.5 | 2.1 | 2010-06-25 13:07:27.7234 | 3.4 | 8.3 | different | 1.234
| ... | ... | ... | ... | ... | ... | ... | ... | ... | ... |

The filename and file format, including the column names can specified upon setting up the Datasource Definition. We can
define `datafile_operations` to convert the file into the expected format, but it is important that whatever
format is chosen, remains consistent. Note that:

1. The filename can be different from `navdata.csv`, and the image directory name `images/` can also be something else,
   as long as it is consistent across deployments. If they are always the same across deployments, they can be defined
   in the pattern, but they can also be defined through a search pattern, for example, an image directory with a
   variable date, `images_20161210/` can be found by setting `mediadir_search="images_*/"`.
2. The navdata file must include a single row for each media item (e.g., image), although it is possible to define a
   preprocessing step that can convert timestamped log files without image references into the desired format.
3. The minimum required columns include: `key`, `pose.lat`, `pose.lon`, `timestamp_start`. Highly recommended columns
   include `pose.alt` and `pose.dep`. You can optionally include any number of additional data columns (
   e.g., `pose.data.sen1...pose.data.senN` above) in which `sen1...senN` can be called anything you like, but are 
   floating point values and can be used to query the DB. Similarly, `data.key1...data.keyN` can be extra key-value 
   pairs and can be strings or numbers, but are less structured and not used for subsequent querying.
5. Column names do not need to match this example exactly. Different column names can be mapped through the Datasource
   Definition, however, using these columns will avoid the additional step required to define a mapping. More often than
   not, existing data file formats and repository structures are map-able without requiring any changes to the data
   repository. The table below shows the expected data column formats.

COLUMN | DESCRIPTION
---- | ----
`pose.lat` | latitude in signed decimal degrees (-90&deg; to 90&deg;, - for S, + for N).
`pose.lon` | longitude in signed decimal degrees (-180&deg; to 180&deg;, - for W, + for E).
`pose.dep` | depth and is a positive float in metres, measuring the depth below the water surface.
`pose.alt` | altitude and is a positive float in metres, measuring the height above the ground (height of the camera above the imaging plane).
`timestamp_start` | should *always be UTC* and should contain date and time (as accurate as possible). Simplest if in single column, but format string can be specified and fields can be merged if date and time are in separate columns at time of import. It is also possible to instead set `timestamp_start_local`, which is the local time (including TZ info), and this will be converted at the time of import based the timezone information included with the datetime string
`key` | must exactly match filenames stored in `images/` directory. This column is used to create the reference URL to the uploaded image/media item.
`pose.data.sen1` ... `pose.data.senN` | are auxiliary data / sensor key-values, and are floating point numbers, and are filterable properties
`data.key1` ... `data.keyN` | are auxiliary info key-values, and can be any data type

The Datasource Plugins make it possible to synchronise with an existing Datasource Repository at scale and also enable
ways to update already imported datasets if they have changed. The update mechanisms can keep the Media Objects that
have already been imported intact, but enable ways to update related Pose objects, while importing any new Media
Objects. This maintains links to any Annotations that have already been applied to the Media Objects, which avoids the
need to export and re-import them if the underlying Deployment survey data requires updating. This commonly occurs in
situations where navigation data may need to be processed and sometimes contains errors, or requires reprocessing.