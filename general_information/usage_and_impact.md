# Data usage and impact

> PAGE UNDER CONSTRUCTION...

The flexible architecture of SQUIDLE+ has enabled the platform to be responsive to the increased interest from the
science and conservation management communities, supporting data-driven decision-making within Australia and
internationally. To date, SQUIDLE+ datasets have seen widespread use, including the generation of comprehensive datasets
capturing essential information about species composition, abundance, distribution, and condition across shallow to
deepsea ecosystems that have been used to track continent-wide declines in shallow reef life over a decade of ocean
warming (\cite{Edgar}), characterize bleaching in sponge communities (\cite {PERKINS_Bleaching}), and document
anthropogenic impacts on deepsea ecosystems  (\cite{TAVIANI_Canyon}). Datasets from SQUIDLE+ have been integrated into
high-level government environmental reporting, such as Australia's State of the Environment reporting (
\cite{Trebleco_SOE}) as well as underpinning monitoring programs at State (e.g. \cite{trove.nla.gov.au/work/250435516})
and Federal government levels (e.g. \cite{Perkins_Timeseries}). Furthermore, SQUIDLE+ has contributed to advancements in
machine learning applications for marine ecosystem monitoring, enabling a more efficient annotation of imagery (e.g.
\cite{10103403}).

Its user-friendly interface and comprehensive datasets have made it a valuable tool for educational purposes, being
incorporated into undergraduate and postgraduate tertiary teaching, delivery of promotional material for student
recruitment (e.g.  https://tinyurl.com/IMASRecruit) and has underpinned >16 postgraduate research students in the past 3
years.

The platform has also fostered collaborations and innovation through national and international projects, including
industry-based research and partnerships with NGOs. In the first three years since going live in mid 2019, datasets from
SQUIDLE+ have contributed to the progress of marine science research and the dissemination of findings to the scientific
community through supporting >AUD5.5M in research projects, resulting in a significant number (>35) of reports and
peer-reviewed papers (Table XXX).

Moreover, SQUIDLE+ enabled the delivery of standardised and interoperable data to third-party platforms. Integration
with initiatives like AODN, Seamap Australia, and TNC Australia Reef Monitoring Program promotes the fairness,
accessibility, and sharing of data and imagery within the marine science community. The ability to export standardised (
see label scheme translation in Section~\ref{sec:semmatic_translation}) indicators meeting the growing demand for
biologically-based metrics, aligned with national and international remoting needs set by (Australian) National Marine
Science Committee (\cite{MNSC}) and Global Ocean Observing System, respectively.

This highlights the platform's growing credibility and recognition as a valuable tool across educational, research and
environmental management settings.

<iframe style="width:100%; height: 800px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSVEicFG3SfcmfjzDgqy_a_iIis-sxkKQRZom_9qAehY0BHykht6HXBPundBOvhkK6T0N8j7t2WubS5/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe>