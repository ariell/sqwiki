# Why SQUIDLE+?

Underwater optical imaging has become an essential tool for studying and managing the world's oceans. It is commonly
used for scientific exploration and characterisation from the shallows to the abyss, to inform the designation and
assessment of marine protected areas, establish environmental baselines, and assess human impacts, including climate
change. However, the rapid collection of imagery has outpaced our ability to efficiently draw useful information and
insights from it.

This has resulted in a variety of annotation tools now being available, including both desktop applications, like
NICAMS (NIWA Image Capture and Management System), VARS (Video Annotation and Reference System;), TransectMeasure (
SeaGIS Pty Ltd), EventMeasure (SeaGIS Pty Ltd), and VIDLIB, as well as web-based platforms such as CoralNet, ReefCloud
and BIIGLE 2.0 (BioImage Indexing, Graphical Labeling, and Exploration). While all of these tools allow annotators to
delineate regions or points in images and label them to morphological or taxonomic categories, they have typically been
focused on specific projects and use cases, with incomplete workflows and limited ability to scale and provide
long-term, reusable annotations for a broader scientific community. To facilitate remote collaboration with adequate
quality control processes, standardised methods and a centralised repository are needed to improve the discoverability,
reusability and validation of annotations while maintaining their links to the underlying imagery.

A centralised web-based annotation tool offers several advantages, including remote collaboration and efficient data
management.

Increasingly, annotation tools are offering automated labelling using Machine Learning (ML), but the ML outputs are
typically limited to the trained models and algorithms provided by the annotation tool backend, making it challenging to
deploy custom algorithms for specific purposes. SQUIDLE+ distinguishes itself by providing a framework for ML
researchers to develop and integrate various algorithms. It is easy to deploy and operationalise trained models
connecting them with end users who can both benefit from the outputs and help validate model performance. This
facilitates cross-disciplinary collaboration and ensures that the ML capabilities remain contemporary with the state of
the art.

This architecture also allows the incorporation of ML models from other open-source initiatives, like FathomNet, which
strives to aggregate images from multiple sources to create a publicly available, curated underwater image training
database. Initiatives like this will be instrumental in generating momentum behind the development of state-of-the-art
ML algorithms, but even these curated databases, are fraught with challenges resulting from a lack of standardisation
between available datasets.

A pervasive and notable challenge when working with annotation data from disparate groups and projects is the
variability in the vocabularies used to annotate the imagery. Imposing a single standardised vocabulary is often
impractical, as different scientists have unique needs and projects. Enforcing such standardisation can lead scientists
to seek alternative tools to maintain flexibility in their data analysis.

![How standard prolipherate](../images/xkcd_standards.png)

In practice, there is little to no standardisation between the vocabularies used for annotation, even within the same
research groups or across different projects. This diversity poses a considerable barrier to data reuse, data
syndication and the large-scale training of ML algorithms.

SQUIDLE+ addresses this challenge by supporting multiple vocabularies that can be standardised using an underlying
translation framework. This allows users to export data in their preferred vocabulary, promoting data reuse,
cross-project synthesis and large-scale training of ML algorithms. It can also collapse fine-resolution labels (e.g.
specific species or substrata facies) to higher-level indicator groupings for management and decision-making purposes
and national or global reporting. For example, Australia's State of the Environment reports,
Australian National Marine Science Committee, United Nations World Ocean Assessment, and
the Global Ocean Observing System have identified a set of indicators
to monitor the state of the ocean and its ecosystems. Traditionally, obtaining these broad indicator-level groupings of
annotations is a significant and arduous undertaking, however, the SQUIDLE+, inbuilt translation framework can provide
these outputs directly, by seamlessly collapsing up the labels to higher level groupings, regardless of the resolution
of the original annotations or the vocabularies that were used (refer to [Key Feature Overview](key_feature_overview.md)).

SQUIDLE+ solves several problems with existing approaches, offering distinct advantages for:

1. **Individual annotators:** Streamlined ingestion of survey data, map-based exploration and
query tools, allow annotators to easily and quickly discover datasets and create subsets based on metadata properties
such as space and time. They can then analyse these subsets using the integrated full-fledged annotation interface with
access to associated plug-ins, like ML algorithms, automated image segmentation processes and other tools. 
2. **Annotation teams and collaborators**: Geographically dispersed scientists can effectively collaborate through
centralised, shared annotations, control over access rights, and the implementation of advanced quality control in their
collaborative workflows; and 
3. **Users of the annotation outputs**: Summarises and reporting tools through to
the large-scale training of ML algorithms, the automated translation between annotation schemes, streamlined synthesis
and a comprehensive backend for flexible querying, filtering, dynamic reporting and exporting of data, ensure that the
utility of the annotations can be extended beyond their original purpose. 

By addressing the challenges of vocabulary variation and enabling flexible ML integration, SQUIDLE+ is a versatile
platform that enhances the efficiency and effectiveness of marine image data management, annotation, and analysis.

Among these, SQUIDLE+ stands out as a tool that a centralised web-based marine image data management, discovery and
annotation platform that offers advanced workflows for analysing marine imagery. It provides tools for spatial and
temporal queries and subsetting survey imagery, expediting the delivery of survey imagery and annotations to end-users.
SQUIDLE+ seamlessly integrates with preexisting cloud storage repositories and offers granular access and sharing
permissions that facilitate collaboration and ultimately the Findability, Accessibility, Interoperability and
Reuseability (FAIR) of marine image data.

The FAIR principles provide a framework for addressing the challenges of discovery and sharing of annotated imagery data
with a diverse group of users and stakeholders. SQUIDLE+ conforms
to the FAIR principles, and effectively extends the utility of marine imagery data beyond its initial purpose, promoting
scientific research and management efforts in the study and conservation of the world's oceans. SQUIDLE+ underpins
Australia's Integrated Marine Observing System's (IMOS) Understanding Marine Imagery (UMI) initiative, which was
originally conceived to help marine scientists in Australia, but it is not limited to Australian datasets or users. The
platform is searchable by the global community and is actively expanding to other regions importing datasets from
different platforms and operators.