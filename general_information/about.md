<style>
    .logo-list {
        text-align: center;
        background-color: #d0d0d0;
        border-radius: 5px;
    }
    .logo-list .logo-img {
        height: 70px;
        margin: 10px 20px;
        display: inline-block;
    }

</style>

# **SQUIDLE+**  

#### **A centralised marine image data management, discovery & annotation platform with vocabulary translation**
#### <i class="fa fa-database fa-fw"></i> Leverage pre-existing cloud storage
#### <i class="fa fa-map-marker fa-fw"></i> Tools for map-based exploration, summary/reporting with advanced annotation workflows
#### <i class="fa fa-share-alt fa-fw"></i> Collaborate & share with humans and algorithms
#### <i class="fa fa-android fa-fw"></i> Comprehensive api: custom reporting tools, dashboards and plugins
#### <i class="fa fa-tags fa-fw"></i> Whole-frame, point & polygon labels
#### <i class="fa fa-image fa-fw"></i> Media-type agnostic (images, videos, large-scale mosaics)
#### <i class="fa fa-check-square fa-fw"></i> QA/QC tools: review flags, batch corrections, cross-validate
#### <i class="fa fa-exchange fa-fw"></i> Semantic translation: seamlessly cross-walk between annotation schemes / vocabularies

[comment]: <> (Harvest data in flexible formats. Once a datasource has been defined, a background service can be scheduled to import new datasets upon upload, automatically. This expedites post-cruise data delivery, eliminating data handling and the need to preprocess data for import. )

[comment]: <> (The centralised repository for annotations maintains links to the underlying survey data, providing tools for map-based exploration, summary/reporting with advanced annotation workflows.)

[comment]: <> (Sharing, collaboration and release of datasets are managed through user groups with granular permissions and data provenance and acknowledgements are tracked throughout the system. Share with other humans or algorithms.)

[comment]: <> (A comprehensive API makes it possible to build custom reporting tools and dashboards and to integrate automated annotation algorithms. The OGC-compliant data outputs facilitate integration with various third-party web-portals.)

[comment]: <> (The workflow includes QA/QC tools, including review flags and a dedicated QA/QC view for rapid, batch correction of labels. Supplementary annotations can be used to facilitate cross-validation between human annotators and between humans and algorithms.)

[comment]: <> (Supports multiple vocabularies, and has a semantic translation framework which enables standardisation in the labelling, allowing annotations to be cross-mapped and exported in a vocabulary of a user’s choosing. This facilitates data reuse, syntheses between projects and large-scale training of machine learning algorithms.)

#### For a more detailed overview, check out the [Key Features Overview](key_feature_overview.md) section

![Squidle+ screenshots](../images/squidle.jpg)

### Overview
SQUIDLE+ is a software platform for the management, discovery and annotation of marine imagery. 
It includes map-based exploration interfaces, advanced annotation workflows, summary reporting tools and analytics 
through a user-friendly frontend and a comprehensive backend. Sharing, collaboration and release of datasets are managed 
through user groups with granular permissions, and annotations can be acquired from human users and/or automated Machine 
Learning (ML) algorithms. 
SQUIDLE+ expedites data delivery and eliminates the need to transfer, copy and/or reprocess data for import. 
It provides a centralised repository for annotations while maintaining links to the originating imagery hosted on various 
pre-existing cloud repositories. The platform offers different annotation modes (whole-frame, points, polygons) and is 
media-type agnostic (images, videos, large-scale mosaics). 
Integrated QA/QC tools enable cross-validation between human annotators and between humans and machine learning algorithms. 
SQUIDLE+ supports multiple standardised or user-defined vocabularies for annotation and also provides tools to translate 
between vocabularies. 
This gives users the flexibility to construct data sets that target specific scientific questions and facilitates data 
reuse, cross-project syntheses, large-scale machine learning training, and broad summaries like National State of 
Environment Reporting. 
It has been developed in close collaboration with an active user community and currently contains datasets from a 
variety of platforms and operators with global extent. To the best of our knowledge, SQUIDLE+ is the largest known 
repository of openly accessible and easily discoverable georeferenced marine images with associated annotations in existence.
This paper details the system architecture and showcases how the tools and features built into the SQUIDLE+ platform 
streamline workflows and facilitates the Findability, Accessibility, Interoperability and Reuseability of marine image data.

> For more information, check out the [Motivation](motivation.md) and [Key Features Overview](key_feature_overview.md) sections

SQUIDLE+ forms the 
foundation of the National Understanding Marine Imagery (UMI) Facility, a new NCRIS-funded, sub facility of Australia’s 
Integrated Marine Observing System. UMI provides a centralised national repository for annotations and currently 
supports a variety of different data sources ranging from autonomous underwater vehicles (AUVs), remotely operated 
vehicles (ROVs), baited remote underwater video (BRUVS), towed underwater imagery and diver photo quadrat data from 
Underwater Visual Census (UVC, eg: Reef Life Survey).

### Videos:

[comment]: <> (##### **NOTE** more &#40;shorter&#41; overview videos are coming soon. In the meantime, here's a presentation: )

<video style="width: 100% !important; height: auto !important;" controls>
  <source src="https://squidle.org/static/video/sq-fair-data2-converted.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>

> **SQUIDLE+: general overview & compliance with FAIR:** the video above is a recording from the FAIR (Findabile, Accessibile, Interoperabile, and 
> Reuseable) session at the AMSA conference (2022). It provides an overview of the system with respect to interoperability and features.

<video style="width: 100% !important; height: auto !important;" controls>
  <source src="https://squidle.org/static/video/sq-ml-case-studies-edited-converted.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>

> **SQUIDLE+: case studies and ML integration:** the video above is adapted from a presentation on 
> bootstrapping automated image analysis through a large and growing repository of high-quality image annotations
> A similar version was presented at an IMOS workshop in 2023.



###Development, Support & Partners
Squidle+, developed by Ariell Friedman ([Greybits Engineering](http://greybits.com.au/)), underpins the Understanding 
Marine Imaging Facility (UMI), which is supported by the 
[Integrated Marine Observing System](http://www.imos.org.au/).
Development has also been supported by the [Schmidt Ocean Institute](http://schmidtocean.org/), the [Japan Agency for Marine-Earth Science and Technology](https://www.jamstec.go.jp/), the
[Nectar science cloud](https://nectar.org.au/new-australian-science-clouds/). Other funders / stakeholders and partners are listed below:

<p class="logo-list">
    <img class="logo-img" src="/static/images/logos/greybits.png">
    <img class="logo-img" src="/static/images/logos/IMOS.png">
    <img class="logo-img" src="/static/images/logos/SOI.png">
    <img class="logo-img" src="/static/images/logos/nectar2.png">
    <img class="logo-img" src="/static/images/logos/aodn_logo.png">
    <img class="logo-img" src="/static/images/logos/geoscience_australia_white.png">
    <img class="logo-img" src="/static/images/logos/jamstec-logo2.png">
    <img class="logo-img" src="/static/images/logos/ncris.png">
    <img class="logo-img" src="/static/images/logos/sims_logo.png">
    <img class="logo-img" src="/static/images/logos/utas-logo.png">
    <img class="logo-img" src="/static/images/logos/csiro_mnf_logo.png">
    <img class="logo-img" src="/static/images/logos/rls-logo.png">
</p>



[comment]: <> (### Features)

[comment]: <> (Below is a quick overview of some of the key features, but for more detailed information, refer to the )

[comment]: <> ([Key Features Overview]&#40;key_feature_overview.md&#41; section.)

[comment]: <> (#### <i class="fa fa-recycle"></i> End-to-end data delivery, management and analysis tools)

[comment]: <> (A bottleneck to analysis is often related to data manipulation, preparation, upload and setup in bespoke workflows.)

[comment]: <> (Squidle+ provides a centralised interface for delivering data direct from the repository and maintains links to )

[comment]: <> (originating survey data. It offers a variety of downstream analysis tools, including map-based )

[comment]: <> (exploration interfaces, summary & reporting tools, advanced image annotation tools and analytics integrations through )

[comment]: <> (the comprehensive API. This expedites data delivery and eliminates the need to offline-process and then manually )

[comment]: <> (manipulate / process / shift survey data around to conduct analyses on a third party platform.)


[comment]: <> (#### <i class="fa fa-database"></i> Flexible data storage & architecture)

[comment]: <> (Requiring that all data is available/uploaded to the centralised web server poses a barrier for adding new data from )

[comment]: <> (other sources, duplicating data that is often already available elsewhere online.)

[comment]: <> (Many national marine observing programs &#40;for example IMOS through the Australian Ocean Data Network &#40;AODN&#41;, or the )

[comment]: <> (Marine Geoscience Data System &#40;MGDS&#41; in the USA, are mandated to put data online in an openly accessible location.)

[comment]: <> (The architecture makes the integration of additional data sources seamless through leveraging existing repositories of )

[comment]: <> (survey data hosted in a variety of cloud storage backends &#40;Amazon Web Services S3, GoogleCloudStorage, Thredds, Apache, )

[comment]: <> (NFS, etc..&#41;. Once a data source has been defined, automated import services can be left to “set and forget” to watch an )

[comment]: <> (online repository and automatically import new data as it is uploaded.)

[comment]: <> (These distributed data storage facilities should be leveraged to reduce data duplication and inconsistencies, and )

[comment]: <> (will also mean that data can be made readily available much more quickly.)

[comment]: <> (Using the framework for interpreting flexible metadata formats, it takes minutes &#40;instead of days&#41; to import)

[comment]: <> (datasets and get them ready for detailed annotation.)


[comment]: <> (#### <i class="fa fa-tags"></i> Flexible annotation schemes / standardisation through translation)

[comment]: <> (There is no one size fits all when it comes to annotation schemes.)

[comment]: <> (Perhaps one of the most important problems that Squidle+ aims to solve is standardisation of the raw annotation data )

[comment]: <> (products. A major challenge when working with annotation data from disparate groups and projects is that there is )

[comment]: <> (little to no standardisation between the annotation schemes/vocabularies used for annotation and these can sometimes )

[comment]: <> (vary within groups from project to project. This poses a barrier to data reuse, syntheses between projects and large-scale )

[comment]: <> (training of ML algorithms. It is also not realistic to impose a single vocabulary upon scientists as their needs and )

[comment]: <> (projects are different. Enforcing a single standardised vocabulary is too restrictive for many scenarios, and tends to )

[comment]: <> (inadvertently drive users towards a splintered/bespoke approach to data analysis using other tools that provide the )

[comment]: <> (flexibility they want. Squidle+ aims to offer flexibility by supporting multiple annotation schemes &#40;vocabularies&#41; and )

[comment]: <> (analysis workflows that can be managed and standardised from within Squidle+ by crosswalking between schemes using )

[comment]: <> (semantic translation tools. This makes it possible to analyse data in a manner that suits users’ needs, and then )

[comment]: <> (export it in a translated target format of their choosing.)


[comment]: <> (#### <i class="fa fa-android"></i> Collaborative labeling between humans and algorithms with QA/QC tools)

[comment]: <> (A comprehensive, feature-rich user group framework enables the sharing, collaboration and release of datasets allowing )

[comment]: <> (for granular permissions &#40;with optional user agreements&#41;, permissions to modify and validate annotations )

[comment]: <> (collaboratively &#40;with traceable authorship and attribution&#41; and mechanisms for public release of data.)

[comment]: <> (There is a substantial API underpinning Squidle+ facilitates interaction with the data using a user's API authentication )

[comment]: <> (token. In a similar way that data can be shared, collaborated on and validated between human users, algorithms can be )

[comment]: <> (set up as "users" of the system and can assist human users in their analysis on datasets that they have been granted )

[comment]: <> (access to through the same sharing framework. This architecture makes it possible to offer a variety of different )

[comment]: <> (externally &#40;or internally&#41; developed automated processing pipelines. It opens up the possibility of connecting )

[comment]: <> (independent machine learning &#40;ML&#41; researchers to real-world ML problems with validated training data, and conversely )

[comment]: <> (provides the marine science user community with access to algorithms that can help bootstrap their analyses. Other )

[comment]: <> (annotation tools that offer automation, are most often wedded to a particular internal ML pipeline. In this )

[comment]: <> (architecture each ML researcher or ML service provider can be in charge of administering their ML integrations. The )

[comment]: <> (comprehensive API and advanced export tools also facilitate integration with external services and databases &#40;eg: ALA, )

[comment]: <> (OBIS, SeamapAustralia, AODN&#41;. )



[comment]: <> (#### <i class="fa fa-film"></i> "Media object" annotation)

[comment]: <> (It enables the same consistent labels to be applied to different media types and offers )

[comment]: <> (flexibility in annotation workflows with different annotation modes eg: whole-frame, points, polygons, )

[comment]: <> (bounding boxes, multiple labels per annotation with tags & comments and is designed to be media-type agnostic, eg: )

[comment]: <> (the same annotation framework can be used for images, videos, large scale mosaics, stereo images, transect quadrats, )

[comment]: <> (and more through the definition of media interpreter plugins. )


[comment]: <> (#### <i class="fa fa-ship"></i> In-field data annotation)

[comment]: <> (Unannotated data from the field can be considered to be a liability in the sense that it often results in huge )

[comment]: <> (repositories of images and video that need to be assessed at a later time. Real-time annotation for video and stills )

[comment]: <> (using the same annotation interface running a local caching database that can be easily synchronised with the online )

[comment]: <> (system post cruise, makes it possible to better leverage time and resources in the field and help to reduce the )

[comment]: <> (“post processing debt”. There are dedicated field server packages that can be tightly integrated to provide realtime annotation tools and event logging)

[comment]: <> (as well as automated data preparation pipelines.)

[comment]: <> (#### <i class="fa fa-graduation-cap"></i> Education & outreach)

[comment]: <> (With the advances in high bandwidth communications and social media, education & outreach activities have become )

[comment]: <> (commonplace on ocean-bound research cruises. The architecture supports the development of simplified interfaces &#40;re-skinning&#41;)

[comment]: <> (makiing it is possible to leverage the development effort in creating science )

[comment]: <> (tools to facilitate outreach goals, opening up the potential to acquire large volumes of crowd-sourced data that can )

[comment]: <> (compliment science objectives and engage the general public. We have already had some successes in this area.)


