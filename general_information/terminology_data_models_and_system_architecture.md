# Useful terminology, Data Models and System Architecture

The tables below provide definitions for relevant Data Model terms. Each Data Models refers to a specific concept in 
the system design and maps to database tables and also have definitions in the API, each with a set of endpoints.
This page contains the following sections:

[TOC]

## System architecture

The SQUIDLE+ backend is built upon open-source software infrastructure. It consists of a Python Flask web server using a
Postgres (with PostGIS) database management system. The database is built through an Object Relational Mapping (ORM)
using SQLAlchemy, which enables flexible migrations and maintenance of the database between updates or upgrades. The
backend presents an extensive [Application Programming Interface (API)](/api/help?template=api_help_page.html), as well as Open
Geospatial Consortium (OCG) web map services. The API facilitates integration with external web services, user-created
scripts & notebooks, and a suite of plugins and tools including libraries to facilitate interoperability, including the
seamless integration of ML algorithms. The figure below provides an overview of the system
architecture.

![System Architecture](../images/sq_system_architecture.png)

## Terms for Datasources
Term | API ref | Description 
--- | --- | --- 
`Platform` | [`platform`](/api/help?template=api_help_page.html#platform) | The equipment/method used to collect the imagery data (eg: a specific AUV, ROV or camera platform) 
`Data Provider` | *N/A* | The organisation, institution or operator in charge of managing the data collected by the `Platform` 
`Datasource Repository` | *N/A* | An accessible, non-ephemeral, (usually online) archive for uploading raw underlying imagery and survey data collected by a `Data Provider`  
`Datasource Plugin` | [`datasource_type`](/api/help?template=api_help_page.html#datasource_type) | A software module that facilitates reading from a `Datasource Repository`
`Datasource Definition` | [`datasource`](/api/help?template=api_help_page.html#datasource) | The parameters that define the structure and format of the data on the `Datasource Repository` and which `Datasource Plugin` to use. Each `Platform` must have at least one `Datasource Definition`.


## Terms for Datasets
Term  | API ref | Description 
--- | --- | --- 
`Media Object` | [`media`](/api/help?template=api_help_page.html#media) | a discreet visual data object from a `Deployment`, eg: image, video, mosaic, etc... `Media Objects` require Interpreter plugins to be viewable and annotatable using the online tools. 
`Campaign` | [`campaign`](/api/help?template=api_help_page.html#campaign) | a group of `Deployments`. Eg: a specific expedition, cruise or data collection initiative 
`Deployment` | [`deployment`](/api/help?template=api_help_page.html#deployment) | a dataset containing a group of `Media Objects` collected from an operation of the `Platform`, eg: a survey, transect or mission 
`Pose` | [`pose`](/api/help?template=api_help_page.html#pose) | the datetime, position and/or orientation of a `Media Object`. A `Media Object` can have multiple `Poses` linked to it. `Poses` are used to georeference EVERYTHING, including `Campaigns`, `Deployments` and `Annotations`.
`Media Collection` | [`media_collection`](/api/help?template=api_help_page.html#media_collection) | a user-defined collection of `Media Objects` grouped for further analysis. Can be a subset of `Media Objects` from one or more `Deployments`
`Annotation Set` | [`annotation_set`](/api/help?template=api_help_page.html#annotation_set) | the definition of how to annotate the `Media Collection`. I.e. the annotation method, parameters and `Label Scheme` to be used. A `Media Collection` can have more than one associated `Annotation Set`
`Label Scheme` | [`label_scheme`](/api/help?template=api_help_page.html#label_scheme) | the chosen list of possible `Labels` for which to apply to `Annotations`. There are multiple `Label Schemes`.
`Label` | [`label`](/api/help?template=api_help_page.html#label) | a discreet concept from the `Label Scheme` that can be assigned to `Annotations`
`Annotation` | [`annotation`](/api/help?template=api_help_page.html#annotation) | a linking of a `Label` to an observation in a `Media Object` (i.e. a `Label` on a `Point` in a frame)
`Point` | [`point`](/api/help?template=api_help_page.html#point) | an annotatable object on a media frame, captures x,y,t and can have multiple Annotations associated with it. A point can also be associated with a bounding box, polygon, pixel map, etc... Each `Point` can have one or more `Annotations`
`Tag` | [`tag`](/api/help?template=api_help_page.html#annotation) | an additional modifier that can be applied to an `Annotation`. Each `Annotation` can have multiple `Tags`.
`Vocabulary Registry` | [`vocab_registry`](/api/help?template=api_help_page.html#vocab_registry) | a queryable database of managed `Vocabulary Elelements` (or concepts) that can be used for semantic mappings. 
`Vocabulary Element` | [`vocab_element`](/api/help?template=api_help_page.html#vocab_element) | a concept or entry in a `Vocabulary Registry` that can be linked to `Labels` from different `Label Schemes` for semantic mappings.

## Annotation properties
Term  | API ref | Description 
--- | --- | --- 
`Polygon` | [`point.polygon`](/api/help?template=api_help_page.html#point) | A list of vertices associated with a `Point` to define a bounding box or polygon
`x`, `y`, `t` | [`point.x,y,t`](/api/help?template=api_help_page.html#point) | the x-y location of a point within a frame, and in the case of video, t denotes the temporal location within the file (runtime or frame number) for the annotation.
`Comment` | [`annotation.comment`](/api/help?template=api_help_page.html#annotation) | a freeform entry that can be associated with an `Annotation`. 
`Likelihood` | [`annotation.likelihood`](/api/help?template=api_help_page.html#annotation) | a parameter defining the uncertainty of the `Label` assigned to an `Annotation`. This is available for machine learning and for human user inputs.
`Needs Review` | [`annotation.needs_review`](/api/help?template=api_help_page.html#annotation) | a flag denoting the `Label` assigned to an `Annotation` requires review. 

## Simplified ERM
The entities listed in the Figure below, are  related to the concepts and models that are outlined in Tables above, 
denoted as "API ref". Note that we are not describing the full list of Data Models, API endpoints or database entities 
here as it would require a level of technical description that confound a cursory understanding of the system, but 
more extensive information can be found in the [API documentation](/api/help?template=api_help_page.html).

![Partial UML diagram](../images/partial_db_uml.png)