# Overview of key features 

This page contains the following sections:

[TOC]


## Annotation workflow & tools

SQUIDLE+ offers flexibility in annotation workflows and different annotation modes e.g., whole-frame, points, polygons,
bounding boxes, multiple labels per annotation with tags and comments and is designed to be media-type agnostic, e.g.,
the same annotation framework can be used for images, videos, large scale mosaics, stereo images, transect quadrats, and
more through the definition of media interpreter plugins. Integrated QA/QC tools, and cross-validation between
annotators enable both human-human and human-algorithm cross-checking and validation. 
The figures below show the UI for applying annotations to image-type Media Objects. 
The image below demonstrates point, polygon and bounding box labels. Bounding boxes and regular polygons (e.g., dodecagons) 
can be quickly created by clicking a point and dragging. Complex polygons can be created by clicking each vertex and 
there are hooks to enable automatic polygon creation based on pixel map functions. The popup shows more information 
about the label including Tags, Comments and Magical Suggestions from ML algorithms. The ticks and crosses over the 
points show agreement with the ML suggestions, and the flag icons show Annotations that have been "flagged for review". 
There are keyboard shortcuts, and advanced options, like (i) auto-select unlabeled points; (ii) auto-advance frames 
to the next unlabeled point; (iii) select all unlabeled points; (iv) batch selection and labelling of points. 

![Annotation UI 1](../images/screenshot_annotation-interface.png)
> Annotation UI showing point labels, polygons and bounding boxes with a random point distribution on an image-type Media Object. Ticks and crosses on points represents agreement with the “Magical Suggestions” from a ML algorithm and the “Flag For Review” feature enabling QA/QC and validation.

![Annotation UI 2](../images/screenshot_annotation-interface2.png)
> Annotation UI showing 5-point "quincunx" point layout  with grid lines overlaid, reference Label exemplars and brightness/contrast adjustments

The Annotation model is agnostic to the Media Object type. The same framework can be applied to video, photo-mosaics and images, provided an appropriate UI plugin has been developed to work that Media Object type and make it viewable in a web browser window. 

> For a more details overview of how to annotate, refer to the [How to Annotate](../instructions_and_tutorials/annotate_media.md) section.

## Sharing & collaboration between users

A comprehensive, feature-rich user group framework enables the sharing, collaboration and release of datasets allowing
for granular permissions (with optional user agreements), permissions to modify and validate annotations
collaboratively (with traceable authorship and attribution) and mechanisms for the public release of data. Users can
create sharing groups with the following properties:

1. **Data Usage Agreement (DUA)**: an option to include a DUA. If provided, and the "require agreement" option is
   activated, members will need to agree to the terms of the linked document before they can access data shared in this
   group.
2. **Public**: an option to make datasets shared in a group publicly viewable (read-only) to all users. Group members
   can still be selectively granted edit permissions.
3. **Restrict data visibility**: an option to restrict the visibility of shared data in a group. Members will only see
   their own datasets and will not be able to see datasets shared by other members. Custodians will be able to see all
   shared datasets. This can be useful when managing data from lots of different contributors who do not want to share
   data between them or for automated labelling services where a user may share a dataset with an algorithm's group for
   training or automated labelling input, but may not wish to share it with all the other users who are members of that
   group.
4. **Require agreement**: an option to enforce the DUA, defined above. Members will be required to agree to usage terms
   before accessing shared data.
5. **Require custodian approval**: an option to prevent members from adding other members without approval. All added
   members will need to be approved by the group custodian and will not have access until approved. If left unchecked,
   members with the relevant permission can add other members without approval.

In addition to the group properties above, users can be added to a group with granular permissions, like whether they
can edit shared datasets or are just read-only, and whether they can add datasets and/ or new members. If a member has
permission to add other members, they will only be able to assign permissions lower than or equal to their own
permission level.

## Automated labelling using Machine Learning algorithms

There is a substantial API underpinning SQUIDLE+ that facilitates interaction with the data using a user's API
authentication token. In a similar way that data can be shared, collaborated on and validated between human users (as
explained in the [section](#sharing--collaboration-between-users) above, ML algorithms can be set up as "users" of the system and
can obtain labelled training datasets, or assist human users in their analysis on datasets that they have been granted
access to through the same sharing framework. This architecture makes it possible to offer a variety of different
externally (or internally) developed automated processing pipelines. It is possible to integrate traditional supervised
ML algorithms, and through the QA/QC and collaboration tools, it is also possible to facilitate Active Learning
workflows, which involves an algorithm prompting a human to interactively review Annotations, with iterative training
cycles in order to maximise ML performance, while minimising the amount of human input. Traditionally, annotation tools
that offer automation, are most often wedded to a particular internal ML pipeline that is baked into the backend. In
this architecture, each ML researcher or ML service provider can be in charge of administering their ML integrations.
This does not preclude the offering of internally provided ML capabilities but provides greater opportunity for
cross-disciplinary collaborations and access to bleeding-edge developments in ML research. Handling algorithms as "
users"  provides a large degree of flexibility and connects independent ML researchers to real-world ML problems with
validated training data; and conversely provides the marine science user community with access to deployed algorithms
that can help bootstrap their analyses. The [sqapi](https://pypi.org/project/sqapi/) library and the [sqbot](https://bitbucket.org/ariell/sqbot)
repository have been built to streamline the deployment of machine learning integration and makes it
relatively straightforward to operationalise ML algorithms into the SQUIDLE+ API.

## Validation & quality control

Validation is an important step in controlling the quality of the data that is captured, and transparency and
standardisation of the Quality Assurance and Quality Control (QAQC) process is essential in facilitating dataset reuse
and synthesis. There are various mechanisms for validation that are supported by the system:

1. **Flag for review**: An Annotation property that allows a user to flag it for review. This feature helps ensure
   high-quality annotation standards by allowing all Annotations that require review to be quickly found. It can also be
   used by an algorithm to request input on Label predictions that it is unsure about (low probabilities). In an
   interactive training and validation cycle, this can be used to facilitate active learning.
1. **Label Exemplar images**: Label Scheme editors can define exemplar Annotations for each Label. Users can quickly
   verify their Annotations by referring to exemplar sample images for each Label. This helps improve the quality of the
   Annotations and also facilitates training (see figures above and below).
1. **Supplementary annotations**: Supplementary Annotation Set can be defined to associate a separate set of Annotations
   with the same Points contained in a base Annotation Set. Supplementary Annotations appear as Label suggestions for a
   given Point and can be used to quickly review, assign or correct suggestions. Supplementary Annotations make it
   possible to cross-validate labels between different users, whether it be human-to-human or human-to-algorithm.
1. **Labelling objective**: An Annotation Set property that captures its intended objective, i.e., whether the labelling
   is using the full Label Scheme (full biodiversity scoring) or whether it is only using part of the Label Scheme (
   targeted scoring). This is important when considering the Annotations for reuse, since the labelling objective can
   influence whether or not a particular dataset is fit for purpose.
1. **Dataset purpose**: An Annotation Set property that captures whether it was intended for science purposes, or if it
   is for experimentation and/or training. This is an important quality indicator that allows filtering out of poor
   quality or experimental labels.
1. **Dataset status**: An Annotation Set property that captures whether it has been finalized and completed or is still
   a work in progress. This helps users to understand if the Annotations are stable or likely to keep changing while
   they are being worked on.
1. **QA/QC status**: An Annotation Set property that captures whether or not it has been QA/QC'd and/or reviewed. This
   helps users to ensure the validity of the Annotations.

All of the properties above help to capture data quality attributes of the Annotations contained in the system. These
properties can all be used to filter the Annotations during export to ensure that they are of high quality and fit for
purpose.

The figure below shows the QA/QC UI, which enables users to view thumbnails of the Annotations, rather than viewing 
each full Media Object. This allows users to view Annotations by Label; quickly apply batch corrections by selecting 
one or more thumbnails and applying new Labels; search Annotations by annotator (user), Tags, Comments and whether or 
not they have been "flagged for review". A row of Label exemplar images is available at the top for easy reference. 
This tool offers an efficient workflow to quickly review the quality of the Annotations and efficiently apply Labels. 
With an ML algorithm in an interactive training and validation cycle, this interface can be used to facilitate active 
learning.

![QAQC UI](../images/screenshot_qaqc.png)
> QA/QC UI showing thumbnails cropped around each annotation from all images in the Annotation Set explorable by Label. This allows a user to quickly review all Annotations for consistency and make batch corrections.

## Vocabularies & Label Scheme translation framework
SQUIDLE+ offers flexibility by supporting multiple Label Schemes (vocabularies) and analysis workflows. These can be managed and standardised from within SQUIDLE+ by cross-walking between schemes using a Label Scheme translation framework. This makes it possible to analyse data in a manner that suits users’ needs, and then export it in a translated target format of their choosing.

SQUIDLE+ also facilitates the extension of existing schemes, which provides a mechanism to define multiple derived schemes from a single base scheme. This enables (i) seamless translation between a base scheme and a derived scheme using the hierarchical tree structure, (ii) efficient mapping by leveraging base scheme vocabulary elements for translation of the derived scheme with no extra effort. 

SQUIDLE+ offers three different mechanisms for translating a source Label into a Label from a target Label Scheme:

1. **Direct mapping**: A method to override translations by predefining a mapping between a source Label and a Label in the target scheme. 
1. **Tree traversal**: Used in special cases where source/target Label Schemes are extended hierarchies of one another. No mapping or translation is required as the source Label will either exist in or will be collapsible up to the parent Label in the target.
1. **Semantic translation**: Matches  Vocabulary Elements from Vocabulary Registries by inferring associations from the semantic mappings in the source and target schemes. Semantic mapping are applied between labels in each scheme and the Vocabulary Elements of one or more central Vocabulary Registries. Each registry may capture different attributes (like taxonomy, morphology or a mixture of the two). A match is found when one or more common Vocabulary Elements are matched between a source Label and a Label in the target Label Scheme. If no match is found, it will move up the hierarchy to attempt to match the parent of the source Label.

The translation method used depends on the source and target Label Schemes. If direct mappings are defined, then they will be used. If the source or target are extensions of one another, then tree traversal will be used. Otherwise, the semantic translation mechanisms will be used to find the best match. Semantic matching will only be possible for schemes that have semantic mappings defined. The mappings need only be defined once, and once defined will enable mapping to any other scheme with semantic mappings. Extended schemes automatically inherit the semantic mappings of the base schemes, which enables high-level mapping without any further effort for newly defined extended Label Schemes. 

[comment]: <> (An overview of the translation framework is provided in Figure \ref{fig:semantictranslation}.)


At the time of writing, SQUIDLE+'s Label Scheme translation tools utilise three Vocabulary Registries, including 
World Register of Marine Species (WoRMs), Codes for Australian Aquatic Biota (CAAB) codes and
the CATAMI morphospecies scheme. WoRMS and CAAB codes have their respective APIs and
web services and a Vocabulary Registry plugin has been defined for each, however, CATAMI is a managed scheme within
SQUIDLE+. The definition of Vocabulary Registries is flexible, and others can be added. Different registries may capture
different attributes (e.g., WoRMs is purely taxonomic, CAAB/Catami are a mix of morphology and taxonomy). This allows
the translation framework to be able to match on both morphology, taxonomy or any other attributes available through the
Vocabulary Registries.

In export user interface, there is an option to "Translate annotations to target Label
Scheme". This enables users to utilise the translation framework to export the Annotation Labels in a target Label
Scheme of their choosing. The Figure below shows a preview of the translation between the RLS
Catalog and the Australian Morphospecies Catalog (AMC), which are two widely used Label Schemes within SQUIDLE+. AMC is
an extension of the CATAMI Label Scheme. 

![Trabnslation preview](../images/example_translation_preview.png)
> Preview of translation between RLS Catalog and Australian Morphospecies Catalog (a CATAMI extension)


In all translation outputs, the original Label will be returned, along
with additional properties containing the translated Label information as well as how the translation was done. I.e., a
property denoting `{method}.{type}.{registry}.{element}` where the `method` is either
`mapping`, `tree_traversal` or `semantic`; the `type` is `direct` for an exact match
or `relative` if it had to traverse up the tree to match a parent Label; and if the `method` is
`semantic`, it will also include the `registry` and `element` that was used to match the Labels. It
can be seen in the translation info in figure above that all of the matches are done using
`semantic` translation. Some are `direct` matches and some are `relative` as the translation engine
had to traverse up the tree to find a match. Some have used the `worms` registry and some have been matched
against the `caab` database. In most cases, a match is found between the RLS Catalog Label and a Label in AMC,
but in some cases, it is matching to a Label in the Catami 1.4 Label Scheme. This is because AMC is an extension of the
Catami 1.4 Label Scheme. If the source and target Label Schemes were extensions of one another, the `method`
would be `tree_traversal`, and the `type` would depend on what level in the hierarchy is matched.



## Geo-referencing, Geospatial map interface & OCG services

The data models contained within the system are geo-referenced using Pose objects, i.e. Poses are linked to Media
Objects, which are contained within Deployments and Media Collections. Deployments are also linked to Campaigns and
Annotations are linked to Media Objects. This makes it possible to georeference all of these data models through
association with their corresponding Pose objects, enabling their presentation spatially for map-based or Geographic
Information System (GIS) applications. The API provides endpoints for working with spatial data and enables spatial
queries (e.g., within a polygon or bounding box, within a proximity of a location, etc...). For example, Campaigns and
Deployments have optimised endpoints for showing the spatial locations of all records through API requests. However,
some data models, for example, Media Objects and Annotations contain vast amounts of records, so working with the entire
database through an API query is infeasible. For this reason, the SQUIDLE+ backend also provides Open Geospatial
Consortium (OGC) compliant Web Map and Web Feature Services (WMS & WFS). This makes it possible to visualise
potentially tens of millions of spatial points on a map using tiled map services, and the API can then provide
feature-rich popups containing contextual information like, sample thumbnails of Media Objects at each location,
breakdowns of Annotation Labels and Pose statistics. In addition, conforming to these widely supported OGC standards
opens up the potential for interoperability with third-party portals and applications. Some examples of the map-based
tools are shown below:

![Explore UI](../images/screenshot_explore_combined.png)
> Map-based explore interface showing a world map, and zooming in with popups, deployment tracks and filtered Pose selection with nearest Media Object previews.