# Annotation on Squidle+

This page covers the following:

[TOC]


Squidle+ offers flexibility in annotation workflows and different annotation modes eg: whole-frame, points, polygons, 
bounding boxes, multiple labels per annotation with tags & comments and is designed to be media-type agnostic, ie: the 
same annotation framework can be used for images, videos, large scale mosaics, stereo images, transect quadrats, and 
more through the definition of media interpreter plugins. Integrated QA/QC tools, and cross validation between 
annotators enables both human-human and human-algorithm cross-checking and validation.
For definition of the terminology used here, refer to 
[Useful Terminology](../general_information/terminology_data_models_and_system_architecture.md). 


## Basic annotation workflow
In general, the workflow for annotation can include the following steps.
This page covers **#3** in the list below, and other steps refer to other sections, where relevant. 

1. **Sign up**: 
   you need a user account in order to create or access annotation data

1. **Explore & Set up**: 
   all imported datasets should be accessible through the [Explore](/geodata/explore) interface (see 
   [here](../general_information/datasources_and_ingesting_from_cloud_repositories.md) for info on importing data). Use the dataset selection 
   tools on the explore page to create a new Media Collection and Annotation Set 
   (see [Managing your datasets](../instructions_and_tutorials/managing_your_datasets.md))

1. **Annotate**: 
   open the Annotation Set and start annotating using the annotation tools, as described below.
   
1. **Share / collaborate**
   use the dataset sharing and collaboration tools to add data to user groups with granular permissions. You can also 
   release data publicly through the sharing tools. See [Usergroups and sharing](../instructions_and_tutorials/usergroups_and_sharing.md) for more.
   
1. **Export**:
   use the export tools to export your annotations in a format of your choice. See [Exporting annotations](../instructions_and_tutorials/export_survey_data_annotations_and_labels.md)

## Definitions and Data Models for annotation
The main components required for  setting up a dataset for annotation and processing are:

1. **Media Collection**: this is a user-defined collection of Media Objects grouped for further analysis. It can be
   built based on filters in the map-based explore tools to create a subset of Media Objects from one or more
   Deployments. A Media Object can only exist in one Deployment, but can be linked to more than one Media Collection.
2. **Annotation Set**: this defines how to annotate the Media Collection. I.e. the annotation method, parameters
   and Label Scheme to be used. The Annotation Set captures additional information, like QA/QC status, labelling
   objective (i.e., is it full-biodiversity scoring or targeted scoring) and dataset objective (i.e., is it intended for
   science purposes, or for education and training). These properties are important quality metrics when considering
   data reuse and sharing. A Media Collection can have more than one associated Annotation Set. This allows users to
   define multiple sets of Annotations on the same set of Media Objects. Annotation Sets can also be linked to each
   other as supplementary Annotation Sets, allowing them to be validated against each other (see Section~\ref{sec:
   validation_qaqc})
3. **Annotations, Points & Labels**: Annotations bind Labels to Points within Media Objects. They define a link
   between a Point, which is a localisation within a Media Object, and associate it with one or more Labels. Annotation
   and Point have a variety of properties, including (but not limited to):
    1. **x, y, t**: the x-y location of a Point within a frame, and in the case of video, t denotes the
       temporal location within the file (runtime or frame number) for the Annotation.
    2. **Point layout controls**: several different mechanisms for point layouts are supported. An annotator can
       choose to manually create targeted points, or select from one of the point layout methods, including (i) N-random
       points; (ii) a regular NxM grid; (iii) 5-point quincunx pattern; or (iv) N-random points in a circular
       distribution (for dealing with illumination patterns). It is also possible to define margin parameters which will
       constrain the point layout methods to specific regions of the image. For example, if an oblique forward facing
       camera shows half scorable benthos and the top half is water column, an annotator can apply a margin to constrain
       all points to the bottom half of the frame.
    3. **Polygon or Bounding box**: a list of vertices associated with a Point to define a bounding box or polygon
    4. **Comment**: a free-form text entry that can be associated with an Annotation.
    5. **Tags**: each Annotation can be associated with multiple Tags. Tags can be thought of as modifiers that can
       be applied to any Annotation across all Label Schemes. They can be useful to capture additional attributes,
       like "Bleached" or "Dead" or "Interesting", which if included each Label Scheme could result in a large number of
       redundant additional Labels to capture all possible combinations.
    6. **Likelihood**: a parameter defining the uncertainty of the Label assigned to an Annotation. This is
       available for machine learning and for human user inputs to convey how much confidence they have in the label
       that is applied. It can also be used to prioritise suggestions from multiple annotators.
    7. **Needs Review**: annotation a flag denoting the Label assigned to an Annotation requires review. This is an
       important QA/QC tool for multi-annotator cross-validation (see Section~\ref{sec:validation_qaqc} for more
       information). Collaborators can quickly find all the Annotations that require review and quickly confirm or
       assign corrections.
    8. **Linked Observations**: Annotations can also be linked between frames. This allows users to capture
       additional observations of the same object without double counting. It can also be used to capture tracking of an
       object between successive keyframes (e.g., in video).
    9. **Targeted or Generated**: a flag denoting whether the Point was manually created by the user, targeting a
       specific object, or if it was auto generated by a point layout method. This helps when using Point labels for
       percent cover estimation in dealing with possible sampling biases from targeted points.
    10. **Multi-Labels**: a Point can have multiple Annotations, each with their own Label. This allows users to
        optionally capture additional attributes. For example, if a user wished to log both the substrata and biota for
        a given Point.
    11. **Label Suggestions**: each Point can also include suggested annotations which are not part of the main
        Annotation Set, but show upon hover of each point. This can be used to receive suggestions from another user,
        whether human or ML, and it is also possible to validate against these suggestions.
    12. **Label Synonyms**: through the semantic translation framework (explained in Section~\ref{sec:
        semmatic_translation}), it is possible to optionally search by Label names in other mapped Label Schemes, and
        also by common names or scientific names, as defined by the Vocab Elements (see Figure~\ref{?})


## Annotation basics
After defining a Media Collection and Annotation Set, open the Annotation Set and click on a Media Object thumbnail to 
open the annotation modal window.

### Applying labels
Annotation is done by selecting one or more point(s) and then assigning a label. If your Annotation Set includes a 
point layout method, it will automatically generate points for you to label. If you're using an Annotation Set that 
requires manual point creation, you'll need to create points to label. See the [Create new points](#creating-new-points-manually) section, below.

* Select one or more point(s) by 
    * Left-clicking them with your mouse, or
    * Using a bounding box: click somewhere on the frame (not on a point) and drag over the points you want to select, or
    * Using an auto-select method (see [Auto-point selection](#using-an-auto-point-select-method) below), or
    * Using the keyboard: `CTRL+A` to select all unlabeled points (note select all will also select unlabelled [Whole-frame annotations](#whole-frame-annotations)) 
* Using the mouse, you can position the cursor over points and see a zoomed in section at the bottom right. In the zoom
  window, there are also options to modify the brightness and contrast of the image.
* With one or more point(s) selected, find the desired label in the Annotate Panel on the right: 
    * Browse the Label Scheme, clicking the plus/minus to expand or collapse nodes, or 
    * Use the search box to find labels. Simply start typing to filter labels. Apply search options by clicking on the 
      magnifying glass icon <i class="fa fa-search"></i>. Search by label name (default) and also by: 
        * Mapped names: vocab_elements from supported vocab_registries (eg: WoRMS species names)
        * Common names: from supported vocab_registries (eg: WoRMS vernaculars)
        * Synonyms: names of labels in other Label Schemes that have been linked through semantic translation mappings
* Apply the selected Label by left-clicking with the mouse or, if searching, with your keyboard using `UP` / `DOWN` arrow keys & `ENTER`.
  The `SPACE` key on your keyboard will expand and collapse nodes (similar to clicking the plus/minus icons)
* Once fully annotated, navigate through Media Objects in a Media Collection by:
    * Clicking the arrow buttons <i class="fa fa-chevron-left"></i>  <i class="fa fa-chevron-right"></i> at the top of the annotation window, or
    * Using the `LEFT` and `RIGHT` arrows on your keyboard, or
    * Using an auto-select annotation select method (see [Auto-point selection](#using-an-auto-point-select-method) below)   
    
![Basic annotation](../images/annotation_labelling_points.gif)

Some tips:

* When selecting points, the focus is automatically assigned to the search box, so you can just start typing to 
  search.
* Using your keyboard to search and apply labels after selecting points is usually quicker than clicking them with 
  the mouse.
* To reload / refresh annotations and resizes the frame (if necessary), click the <i class="fa fa-refresh"></i> icon, or 
  use the keyboard shortcut: `CTRL+R`. This can be useful for collaborative labelling or if you resize the annotation window.


### Using an auto-point select method
The auto-point select methods allow you just focus on labelling using your keyboard without needing to use your mouse.
It will automatically select points for you and focus the zoom window. Once a label has been applied, it will move to 
the next unlabeled point.

* Click the <i aria-hidden="true" title="cog" class="fa fa-cog"></i> icon at the top of the Label Scheme panel to expand 
  the annotation options. Click one of the auto point select options:
    * **Auto-skip labeled frames & points**: will advance to next unlabelled point in Media Collection, auto advancing the media objects.
    * **Auto-select unlabeled points in current frame**: will select the next unlabelled point in the current frame
    * **Manually select points and frames**: will not auto-advance, and allow for manual point selection
  
![Annotation options](../images/annotation_options.png)

### Adding tags and comments
* Right-click an annotated point
* Select / search tags to apply (optional)
* Type a freeform text comment (optional)
* Click "SAVE" for each annotation that you have edited 

![Tags and comments](../images/annotation_tags_and_comments-cropped.gif)


### Creating new points (manually)
Depending on what type of point layout method you have chosen for your Annotation Set, you may not want to do this,
since your points may be auto-generated. To create points, you need an Annotation Set that allows manual point creation.
See [Media Collections and Annotation Sets](managing_your_datasets.md).

* To add a new point, double-click somewhere on the frame (this will only work if the Annotation Set allows it).
* Label as described in the [Applying labels](#applying-labels) section, above.

![Creating points](../images/annotation_creating_points.gif)

### Whole-frame annotations
Whole-frame annotations can be annotated in the same fashion as [Applying labels](#applying-labels). 
You can use the "plus" symbol to add additional whole-frame labels where more than one is required. These 
whole-frame labels are often used to provide an overall description who the entire image (e.g. bedform or relief). 

### Creating Bounding boxes & Polygons

* To create a bounding box, click on a point and drag your cursor

![Creating bounding boxes](../images/annotation_bounding_box.gif)

Polygons:

* To draw a polygon around a point, right-click a point to bring up more options and click the "Polygon" button
* Click on the frame to add vertices to your polygon
* Double-click to finish drawing

![Creating polygons](../images/annotation_polygon.gif)

To delete a bounding box or polygon, draw a small bounding box. Below a minimum size, the bounding box goes red and is 
deleted.  

### Moving annotations
If an annotation (e.g. [Creating new points (manually)](#creating-new-points-manually) or [Creating Bounding boxes & Polygons](#creating-bounding-boxes-polygons)) 
is not in the correct position it can be moved (although not recommended for automatically generated points). This is done by right-clicking on polygon and selecting 
"+Move" button at bottom of window. Double-click to finish move.

### Multiple labels per annotation
...
