# Importing label schemes
> PAGE UNDER CONSTRUCTION...

Ingest, manage and synchronise Label Schemes from remotely managed datafiles using the Label Scheme import endpoints.
This enables batch imports of Labels into Label Schemes, and also facilitates synchronisation of externally managed
Label Schemes from third-party vocabulary services