# Export of Survey data, Annotations and Labels

This page contains the following sections:

[TOC]

The API offers a variety of mechanisms to work with data. While all of the Data Models have API endpoints that will
retrieve data out of the system (through `HTTP GET` requests), there are some specialised export endpoints that
provide more flexibility and fine-grained control of how to export data. The export endpoints also enable
transformations on the outputs which can convert the outputs into different formats, similar to the
`datafile_operations` outlined in [Datasources and Ingesting from Cloud Repositories](../general_information/datasources_and_ingesting_from_cloud_repositories.md). The export endpoints also provide a
mechanism to define templates that can provide different formats for the exported data. Templates make it possible to
return the data in different formats, for example, JSON, CSV, HTML table or a more complex formatted HTML document that
can be used to present data from the API into a user interface.


## Annotation export

Retrieve Annotations with filters and fine-grained control over exactly what properties are returned. For example,
whether to include Point information (x-y locations of annotations, bounding boxes, polygons, etc..), Media Object
properties (e.g., Media Key, Campaign, Deployment, Platform info), Pose information (i.e. latitude, longitude, depth,
datetimes, etc...) or whether to translate the Labels to another target Label Scheme.

The export tools give fine-grained control over what properties are returned and enable flexible output formats with 
filtering and templates and a variety of options. The figure below shows the export options for an 
Annotation Set. It shows Datasource attribution information, as well as how the dataset has been shared with a user 
(along with any Data Usage Agreements that may apply). The user has the ability to specify exactly which properties 
appear in the output, by expanding each of the sections for more detailed options. Clicking on the "ADVANCED" button, 
allows the user to see exactly what parameters are being passed to the API. This is useful for "power users" who may 
wish to tweak the output formats or template (with options to change between CSV, JSON, HTML table, etc..) or as a 
tool to help build API queries that can then be executed from an external script (R or Python) for use in external 
integrations or reporting tools.

![Export UI](../images/screenshot-export_options.png)
> Annotation Set export options showing fine-grained control over output columns as well as advanced options that help to build API queries.

## Survey data export

There are also endpoints that facilitate the export of the survey data, specifically all data contained within a
Deployment. This endpoint makes it possible to represent all data that have been imported from the external/remote
Datasource Repositories in a consistent and standardised output, regardless of the source format of the data that it was
ingested from. This helps to improve the findability, accessibility and interoperability of all imported datasets
contained within the system.

## Annotation summary export

Output summaries of annotations using a variety of filters, to look at the Annotation count breakdown by Label, Media
Collection, Deployment, Campaign, User, User Group, User Affiliation, etc... These endpoints also support Label Scheme
translations.

## Label Scheme export

Label Schemes can be managed from within the system or synchronised with externally managed vocabularies. The export
endpoints enable ways to download Label Schemes in a standardised and consistent way to be used in other annotation
platforms or tools. It is also possible to apply templates to the exported Labels contained within a Label Scheme to
preview graphical catalogues or labelling guides. This endpoint also supports Label Scheme translations, which opens up
the possibility of using the translation framework in third-party applications as a web-service providing mappings
between Label concepts.