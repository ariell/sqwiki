# Terms of Reference
> **FOR THE UNDERSTANDING OF MARINE IMAGERY STEERING COMMITTEE**

[TOC]

## 1. Background

Understanding of Marine Imagery (UMI) provides a centralised national repository for annotations and currently supports
a variety of different data sources ranging from autonomous underwater vehicles, remotely operated vehicles, drop
cameras, towed underwater imagery and diver photo quadrat data. SQUIDLE+ forms the foundation of the UMI Facility. UMIs
mission is to deliver digital infrastructure for the marine imagery community to generate and share quantitative,
trusted information for environmental reporting by managers, policymakers, researchers and other stakeholders including
the general community (“End Users”).

Specifically, UMIs objectives are to:

1. Develop and maintain an open-source centralised and interoperable web-based marine image data management, discovery
   and annotation platform that offers advanced workflows for analysing marine imagery;
2. Expedite the flexible delivery of FAIR marine imagery, ensuring that quality-assured annotations are curated,
   standardisable and made accessible to end-users for managing marine jurisdictions.

The facility offers an advanced and dynamic platform that revolutionises marine imagery data management and
collaboration through its innovative approach to content organisation. Serving as a comprehensive solution for diverse
users, SQUIDLE+ seamlessly integrates powerful data storage, efficient retrieval, and collaborative workflows. Its
intuitive interface and cutting-edge features empower users to effortlessly generate, organise and share vast amounts of
information, fostering agile collaborations and the dynamic delivery of trusted and current data for decision-making.
Whether utilised for scientific research, supporting decision-making, or creative endeavours, SQUIDLE+ stands at the
forefront of technology, offering a unified ecosystem where users can easily and accurately unlock the full potential of
their marine imagery data.

This UMI directly addresses the National Marine Science Plan 2015-2025, which envisions an interoperable, online network
of marine and coastal data resources supporting science, education, and management needs. It fully embraces a federated,
standards-based approach to give Australian marine science a competitive edge.

The UMI Steering Committee has been established as part of the UMI facility instigated by Greybits Engineering and
administered by the Sydney Institute for Marine Science (SIMS) in collaboration with formal partners from the Institute
for Marine and Antarctic Studies, CSIRO, Geoscience Australia, South Australian Research and Development Institute,
University of Western Australia, NSW Department of Primary Industries and NSW Department of Climate Change, Energy, the
Environment and Water.

Relevant documents can be accessed at: [https://squidle.org/wiki](https://squidle.org/wiki)

## 2. Purpose of the Steering Committee

The Steering Committee serves as a strategic governing body, established to provide guidance, oversight, and direction
for the successful development, implementation, and evolution of UMI. Comprising key stakeholders, experts, and
decision-makers, the committee is dedicated to ensuring the achievement of UMIs objectives and maximising its impact.

## 3. Duties and Responsibilities of the Committee

The UMI Steering Committee is dedicated to steering the facility towards excellence, innovation, and positive impact,
ensuring that UMI remains a leading solution in its field. The steering committee will provide:

**Strategic Direction:** The Steering Committee is responsible for defining and refining the strategic vision of UMI.
This includes ensuring an enduring, sustainably funded facility and aligning the facility's development with the mission
and objectives. The Committee will assist with the development of a five-year [Strategic Plan](strategic_plan.md) to be 
reviewed annually.

**Governance and Oversight:** The committee plays a crucial role in establishing robust governance structures for UMI,
ensuring that decision-making processes are transparent, accountable, and aligned with the facility's mission. It
monitors the implementation of policies and safeguards to maintain ethical standards and compliance with FAIR data
principles([see Wilkinson et al. 2016](https://www.nature.com/articles/sdata201618)).

**Stakeholder Engagement:** Recognising the importance of a diverse range of perspectives, the committee actively
engages with stakeholders, including users, partners, and the broader community. Feedback mechanisms that are detailed
in the [Strategic Plan](strategic_plan.md) are established to incorporate valuable insights into users of the facility.

**Performance Evaluation:** Regular assessment of UMIs performance against the predefined metrics and benchmarks
identified in the [Strategic Plan](strategic_plan.md) is a key responsibility of the committee. It aims to identify areas for improvement,
innovation, and adaptation to changing user needs and technological advancements.

## 4. Guiding Principles

The UMI Steering Committee operates with the following guiding principles:

* **Collaboration:** Encouraging collaboration and partnerships that contribute to the growth and success of UMI.


* **Inclusivity:** Ensuring that UMI reflects a diverse range of perspectives and meets the needs of a broad user base.


* **Innovation:** Fostering a culture of continuous improvement and innovation to enhance the functionality and
  relevance of SQUIDLE+.


* **Transparency:** Upholding transparent communication and decision-making processes to build trust among stakeholders.


* **Ethical Considerations:** Prioritising ethical considerations in the development and deployment of UMI with a
  commitment to FAIR data practices and interoperability.

## 5. General

### 5.1. Committee Membership

The Committee shall be comprised of:

1. A Chair nominated and elected by committee members annually.
2. Up to two vice-chairs, ready to act as the Chair if the Chair is not available.
3. Up to 12 members (including Chair and vice chairs) representing relevant organisations operating across and within
   relevant jurisdictional boundaries and with a broad understanding of marine imagery and representation including
   researchers, decision-makers, industry and other end users.
4. A Greybits technical lead (non-voting) for advice related to the development, maintenance, and management of UMI.
5. A representative from IMOS
6. Membership is for three years and members can be re-elected for additional terms.

### 5.2. Chair

The Chair shall convene the Committee meetings.

If the designated Chair is not available, then the Chair will nominate an Acting Chair who will be responsible for
convening and conducting that meeting. The Acting Chair is responsible for informing the Chair as to the salient
points/decisions raised or agreed to at that meeting.

### 5.3. Agenda Items

All Committee agenda items must be forwarded to the Chair by COB 7 working days prior to the next scheduled meeting.

The Committee agenda, with attached meeting papers, will be distributed at least 3 working days prior to the next
scheduled meeting.

The Chair has the right to refuse to list an item on the formal agenda, but members may raise an item under ‘Other
Business’ if necessary and as time permits.

### 5.4. Minutes and Meeting Papers

The minutes of each Committee meeting will be prepared by the Chair or their delegate.

Full copies of the Minutes, including attachments, shall be provided to all Committee members no later than 14 working
days following each meeting.

By agreement of the Committee, out-of-session decisions will be deemed acceptable provided a Quorum of members
participate in the decision. Where agreed, all out-of-session decisions shall be recorded in the minutes of the next
scheduled Committee meeting.

The Minutes of each Committee meeting will be monitored and maintained by the Chair as a complete record as required
under provisions of the Archives Act 1983.

### 5.5. Frequency of Meetings

The Committee shall meet twice yearly on agreed dates during March and September each year. Additional meetings may be
held as required or by agreement of the committee.

### 5.6. Quorum

A minimum of six Committee members (inclusive of the Chair or nominee) is required for the meeting to be recognised as
an authorised meeting for the recommendations or resolutions to be valid.

### 5.7. Implementation of Agreed Changes

Changes to UMI identified or approved by the Committee will be implemented by Greybits Engineering (or an appropriate
subcontractor).

### 5.8. Document Control

| Version | Revision Date | Author | Action |
|---|---|---|---|
| Draft | 18/12/2023 | Jacquomo Monk | Developed TOR |
| Final | 17/06/2024 | Jacquomo Monk | Finalised TOR |
| Final | 18/06/2024 | Ariell Friedman | Published TOR |
