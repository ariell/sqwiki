# Strategic Plan
> **FOR THE UNDERSTANDING OF MARINE IMAGERY STEERING COMMITTEE**

[TOC]

## 1. Background
### 1.1 Overview
Understanding of Marine Imagery (UMI) provides a centralised national repository for annotations and currently supports
a variety of different data sources ranging from autonomous underwater vehicles, remotely operated vehicles, drop
cameras, towed underwater imagery and diver photo quadrat data. SQUIDLE+ forms the foundation of the UMI Facility. UMIs
mission is to deliver digital infrastructure for the marine imagery community to generate and share quantitative,
trusted information for environmental reporting by managers, policymakers, researchers and other stakeholders including
the general community (“End Users”).

### 1.2 Motivation

Marine imagery data within the UMI is recognised as a priority for marine park and biodiversity conservation discovery
and monitoring, providing baselines and benchmarks with which to measure management success. UMI provides an intuitive
interface and cutting-edge features that empower users to effortlessly generate, organise and share vast amounts of
information, fostering agile collaborations and the dynamic delivery of trusted and current data for decision-making.

This need directly addresses the National Marine Science Plan 2015-2025, which envisions an interoperable, online
network of marine and coastal data resources supporting science, education, and management needs. It fully embraces a
federated, standards-based approach to give Australian marine science a competitive edge.

## 2. Vision

To revolutionise marine imagery data management and maximise the utility of marine images and annotations through a 
powerful, flexible, and collaborative platform, integrating advanced annotation workflows, machine learning, and 
geospatial exploration tools. This enables stakeholders to access and utilise high-quality, annotated data for informed 
environmental decision-making, fostering a global community of scientists, researchers, and conservationists working 
together to understand and protect our oceans.

## 3. Objectives

UMIs overarching objectives are to:

[comment]: <> (1. Develop and maintain an open-source centralised and interoperable web-based marine image data management, discovery)

[comment]: <> (   and annotation platform that offers advanced workflows for analysing marine imagery;)

[comment]: <> (2. Expedite the flexible delivery of FAIR &#40;Findable, Accessible, Interoperable, Reusable&#41; marine imagery, ensuring that)

[comment]: <> (   quality-assured annotations are curated, standardisable and made accessible to end-users for managing marine)

[comment]: <> (   jurisdictions.)


1. **Develop Open Source Infrastructure**: Establish a fundamental, open-source infrastructure to create a trusted national repository for marine imagery annotations. This infrastructure will align with FAIR data principles and include comprehensive tools for annotation, discovery, exploration, validation, sharing, and exporting of data. 
2. **Facilitate Open Sharing**: Enable the open sharing and public release of high-quality annotation data through a centralised repository, incorporating software tools for ingesting both existing and new data streams. 
3. **Integrate Data from Diverse Sources**: Provide robust tools to quickly ingest data from various current and new file storage repositories, ensuring the incorporation of data from diverse marine imaging platforms and programs. This will include maintaining links to the original imagery by leveraging pre-existing, publicly accessible file storage repositories. 
4. **Standardise Marine Image Metadata**: Standardise metadata for marine images from disparate sources and repositories, offering consistent workflows across different platforms and data collection programs.
5. **Streamline Workflows**: Expedite the delivery of annotated survey data by streamlining workflows, eliminating the need for data duplication during annotation and post-processing. This includes creating a collaborative environment for project management with multiple annotators and users, incorporating automated outputs from machine learning algorithms, and enabling cross-validation between human annotators and between humans and algorithms using QA/QC tools to ensure label consistency. 
6. **Support Multiple Vocabularies**: Support various vocabularies for marine image annotation while facilitating standardisation through semantic translation tools. These tools will allow cross-walking between vocabularies, enabling users to answer specific scientific questions and facilitating data reuse, synthesis between projects, and large-scale training of machine learning algorithms. 
7. **Third-Party Integration**: Provide services that facilitate integration into third-party portals, such as AODN and Seamap Australia, enhancing the accessibility and utility of the annotated data. 
8. **High-Level Reporting and Dashboards**: Facilitate the creation of high-level reporting and dashboard-style interfaces to improve science communication, inform policy decisions, and support the management of Sea Country. 
9. **Enhance Data Discovery and Exploration**: Develop advanced tools for the discovery and exploration of annotation data, allowing users to efficiently search, filter, and visualise data to support their research and analysis needs. 
10. **Promote Data Interoperability and Reuse**: Ensure the data repository and associated tools promote interoperability and data reuse, making it easier for researchers to integrate and synthesise data from multiple projects and sources. 
11. **Foster Community Engagement and Collaboration**: Build a vibrant community of users and contributors by providing platforms and tools that facilitate collaboration, sharing of best practices, and collective problem-solving in the field of marine image annotation.


## 2. Business case and funding required for UMI

### 2.1. Business case

UMI presents a compelling business case as a comprehensive, open-source, centralised, and interoperable web-based
platform for curating and managing marine imagery datasets. This platform ensures the flexible delivery of FAIR marine
imagery, ensuring that quality-assured annotations are curated, standardised, and accessible for streamlined reporting
and improved decision-making.

The innovative SQUIDLE+ platform within UMI enhances the efficiency and quality of marine imagery data management. By
offering advanced annotation workflows and integrating AI technologies, SQUIDLE+ improves data annotation quality while
reducing manual effort, leading to significant cost savings and time optimisation for users. Additionally, SQUIDLE+
fosters collaboration and knowledge sharing through its interoperable design, enabling seamless integration with other
nationally important platforms like [Seamap Australia](https://seamapaustralia.org/), and promoting a data-driven
approach to marine conservation and management. UMI collaborates closely with research agencies and government entities
to ensure the platform's ongoing development meets user needs.

Since its inception in July 2020, UMI has become the largest repository of openly accessible, easily discoverable
georeferenced marine images with associated annotations in the world. It contains survey imagery data from 20 distinct
externally hosted cloud storage repositories, including over 8.5 million images with global coverage. With nearly 2,000
registered users contributing over 3.5 million annotations, the platform represents an estimated value of ~AUD20 million
in survey data and ~AUD2 million in associated annotations, all made available through the SQUIDLE+ software platform in
accordance with FAIR data principles.

### 2.2. Funding

The ongoing development and support of UMI requires XXX FTE per year with a total cost of ~AUDXXX k per year. Database,
website and AI classifiers hosting is currently provided by the ARDC NeCTAR Research Cloud, to enable research and
services of national importance.

The majority of the ongoing development and support for UMI is funded by IMOS. Importantly, this investment by IMOS now
underpins, as of June 2024, ~AUD16M in research initiatives funded
through; [Climate Change AI](https://www.climatechange.ai/), ARC Linkage
project ([LP220200949](https://www.arc.gov.au/sites/default/files/2023-06/LP22_round_2_nit_statements.pdf)), Parks
Australia, [Our Marine Parks Grants](https://parksaustralia.gov.au/marine/management/partnerships/our-marine-parks-grants/)
, numerous NESP Marine and Coastal Hub projects ([2.1](https://www.nespmarinecoastal.edu.au/project/2.1/)
, [2.2](https://www.nespmarinecoastal.edu.au/project/2.2/), [2.3](https://www.nespmarinecoastal.edu.au/project/2.3/)
, [3.6](https://www.nespmarinecoastal.edu.au/project/3-6/), [4.20](https://www.nespmarinecoastal.edu.au/project/4-20/)
, [4.21](https://www.nespmarinecoastal.edu.au/project/4-21/)), [ReefLifeSurvey](https://reeflifesurvey.com/)
and [TNC Australia](https://www.natureaustralia.org.au/).

## 3. Short- and long-term goals of the Steering Committee

The UMI Steering Committee has been established as part of the UMI facility instigated by Greybits Engineering and
administered by the Sydney Institute for Marine Science (SIMS) in collaboration with formal partners from the Institute
for Marine and Antarctic Studies, CSIRO, Geoscience Australia, South Australian Research and Development Institute,
University of Western Australia, NSW Department of Primary Industries and NSW Department of Climate Change, Energy, the
Environment and Water.

The role of the Committee is to provide strategic direction, governance and leadership to ensure the purpose of UMI is
maintained. This committee provides a guide for UMI to ensure it remains relevant and an integral component of the
national and international data infrastructure support framework in marine science. For more information on the
committee, see the [Terms of Reference](terms_of_reference.md).

### 3.1. Short-term goals (1-2 years) for UMI

#### 3.1.1. Development and integration of AI annotation tools

The current and future need for the development and integration of AI annotation tools will continue to grow as the
volume and diversity of marine imagery data within UMI expands. This is needed to reduce the cost and enhance the
quality of annotation data relevant to conservation and management efforts.

#### 3.1.2. Integration of new imaging platforms/data sources

The integration of new imaging platforms and data sources is essential for expanding the spatial and temporal coverage
of UMI, enabling a more comprehensive understanding of marine ecosystems and endusers.

#### 3.1.3. Sharing workflows and training materials

There is a need to publicise and share UMI workflows to encourage further standardised and cost-effective quality
control of these important data and resources. This will also increase the regular user base and end users.

### 3.2. Long-term goals (3-5 years) for UMI

#### 3.2.1. Develop a consistent, balanced funding model

The data and services provided by UMI have now been recognised at State (e.g. NSW DPI) and Federal (e.g. Parks
Australia) levels as vital for marine decision-making and biodiversity conservation discovery and monitoring. Developing
a consistent funding model to maintain services will deliver cost-efficiency benefits to local, national and
international marine science and management.

#### 3.2.2. Continued development and improvement services to serve both local and global communities

Develop and improve the efficiency of existing services to quality assurance and archive annotations, synthesise data,
and syndicate data products for environmental reporting globally.

#### 3.2.3. Imagery storage

Web-accessible storage and backup of marine imagery continues to be a challenge. It should be a high priority for the
UMI committee to secure national imagery storage for high-priority datasets.

#### 3.2.4. Support the development of environmental reporting at state and federal levels

Data summaries and synthesis products are a vital step to improving the efficiency of environmental reporting. The UMI
development team has expertise in syndicating and interactively displaying summaries and synthesis products for
environmental reporting.

By offering development and maintenance support for interactive data displays to data contributors and end users
interested in creating their own dashboards, UMI can enhance its capacity for data syndication.
