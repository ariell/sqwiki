# SQWIKI README #

Welcome to the SQWIKI git repository. 

### What is this repository for? ###

* This is where the documentation for Squidle+ lives.
* It provides an easy wat to contribute to and configure the man pages for SQ+

### How do I contribute? ###

* Fork this repository ([how to Fork a repo](https://support.atlassian.com/bitbucket-cloud/docs/fork-a-repository/))
* Make desired contributions changes and commit/push those changes
* Create a pull request back to the original repo for approval

The above steps can all be done using the bitbucket online tools. 
No need to install git or edit offline (although there are IDEs and tools that may make editing easier).
Create a bitbucket account, if you don't already have one, and start contributing!

### Contribution guidelines ###
* **Structure**: The docs are divided into section by directories and documents. Eg: `annotation/` is a section and 
`basic_annotation.md`, nested beneath it is a document. 

* **Document / directory naming**: The table of contents for the docs wiki is auto generated from the section / document names. The names should be all
lower case and no spaces. Underscores `_` are automatically converted to spaces on the TOC menu.

* **Formatting documents**: All docs pages are written using markdown ([Learn Markdown](https://bitbucket.org/tutorials/markdowndemo))

### Tips and tricks
#### Adding images
Add images to the `images/` directory. Those can then be inserted into documents using the standard Markdown image tag using a relative URL:
```markdown
![Image title](../images/name_of_image.png)
```

#### Adding relative links to other wiki documents
Documents can be linked to with relative links using standard Markdown link tags with a relative URL:
```markdown
[Text for link](../section_name/document_title.md)
```

#### Adding a Table of Contents
By default, all headers will automatically have unique id attributes generated based upon the text of the header. 
To insert a table of contents with links to the headers in that document, add the following tag / marker:
```markdown
[TOC]
```